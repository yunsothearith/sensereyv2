<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> President

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'PersidentController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'PersidentController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PersidentController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'PersidentController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'PersidentController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PersidentController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'PersidentController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'PersidentController@updateStatus']);
});	