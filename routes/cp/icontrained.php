<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Icontrained

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'IcontrainedController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'IcontrainedController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'IcontrainedController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'IcontrainedController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'IcontrainedController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'IcontrainedController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'IcontrainedController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'IcontrainedController@updateStatus']);
});	