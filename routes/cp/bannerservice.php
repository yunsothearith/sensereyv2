<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannerserviceController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannerserviceController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannerserviceController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannerserviceController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannerserviceController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannerserviceController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannerserviceController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannerserviceController@updateStatus']);
});	