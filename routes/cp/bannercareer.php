<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannercareerController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannercareerController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannercareerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannercareerController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannercareerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannercareerController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannercareerController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannercareerController@updateStatus']);
});	