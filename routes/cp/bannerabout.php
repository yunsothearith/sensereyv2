<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BanneraboutController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BanneraboutController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BanneraboutController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BanneraboutController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BanneraboutController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BanneraboutController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BanneraboutController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BanneraboutController@updateStatus']);
});	