<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' => 'auth', 'namespace' => 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		Route::group(['as' => 'user.',  'prefix' => 'user', 'namespace' => 'User'], function () {
			require(__DIR__.'/user.php');
		});
		Route::group(['as' => 'bannerabout.',  'prefix' => 'bannerabout', 'namespace' => 'Bannerabout'], function () {
			require(__DIR__.'/bannerabout.php');
		});
		Route::group(['as' => 'bannerservice.',  'prefix' => 'bannerservice', 'namespace' => 'Bannerservice'], function () {
			require(__DIR__.'/bannerservice.php');
		});
		Route::group(['as' => 'bannerproject.',  'prefix' => 'bannerproject', 'namespace' => 'Bannerproject'], function () {
			require(__DIR__.'/bannerproject.php');
		});
		Route::group(['as' => 'bannerteam.',  'prefix' => 'bannerteam', 'namespace' => 'Bannerteam'], function () {
			require(__DIR__.'/bannerteam.php');
		});
		Route::group(['as' => 'bannercareer.',  'prefix' => 'bannercareer', 'namespace' => 'Bannercareer'], function () {
			require(__DIR__.'/bannercareer.php');
		});
		Route::group(['as' => 'bannergallery.',  'prefix' => 'bannergallery', 'namespace' => 'Bannergallery'], function () {
			require(__DIR__.'/bannergallery.php');
		});
		Route::group(['as' => 'bannerblog.',  'prefix' => 'bannerblog', 'namespace' => 'Bannerblog'], function () {
			require(__DIR__.'/bannerblog.php');
		});
		Route::group(['as' => 'bannercontact.',  'prefix' => 'bannercontact', 'namespace' => 'Bannercontact'], function () {
			require(__DIR__.'/bannercontact.php');
		});

		Route::group(['as' => 'slide.',  'prefix' => 'slide', 'namespace' => 'Slide'], function () {
			require(__DIR__.'/slide.php');
		});
	
		Route::group(['as' => 'partner.',  'prefix' => 'partner', 'namespace' => 'Partner'], function () {
			require(__DIR__.'/partner.php');
		});
		Route::group(['as' => 'team.',  'prefix' => 'team', 'namespace' => 'Team'], function () {
			require(__DIR__.'/team.php');
		});
		Route::group(['as' => 'blog.',  'prefix' => 'blog', 'namespace' => 'Blog'], function () {
			require(__DIR__.'/blog.php');
		});
		Route::group(['as' => 'projectprocess.',  'prefix' => 'projectprocess', 'namespace' => 'Projectprocess'], function () {
			require(__DIR__.'/projectprocess.php');
		});
		Route::group(['as' => 'history.',  'prefix' => 'history', 'namespace' => 'History'], function () {
			require(__DIR__.'/history.php');
		});
		Route::group(['as' => 'persident.',  'prefix' => 'persident', 'namespace' => 'Persident'], function () {
			require(__DIR__.'/persident.php');
		});
		Route::group(['as' => 'vision.',  'prefix' => 'vision', 'namespace' => 'Vision'], function () {
			require(__DIR__.'/vision.php');
		});
		Route::group(['as' => 'mission.',  'prefix' => 'mission', 'namespace' => 'Mission'], function () {
			require(__DIR__.'/mission.php');
		});
		Route::group(['as' => 'certifiate.',  'prefix' => 'certifiate', 'namespace' => 'Certifiate'], function () {
			require(__DIR__.'/certifiate.php');
		});

		Route::group(['as' => 'service.',  'prefix' => 'service', 'namespace' => 'Service'], function () {
			require(__DIR__.'/service.php');
		});

		Route::group(['as' => 'project-test.',  'prefix' => 'project-test', 'namespace' => 'Project'], function () {
			require(__DIR__.'/project-test.php');
		});
	
		Route::group(['as' => 'project-image.',  'prefix' => 'project-image', 'namespace' => 'Projectimage'], function () {
			require(__DIR__.'/project-image.php');
		});
		Route::group(['as' => 'gallery.',  'prefix' => 'gallery', 'namespace' => 'Gallery'], function () {
			require(__DIR__.'/gallery.php');
		});
		Route::group(['as' => 'trained.',  'prefix' => 'trained', 'namespace' => 'Trained'], function () {
			require(__DIR__.'/trained.php');
		});
		Route::group(['as' => 'icontrained.',  'prefix' => 'icontrained', 'namespace' => 'Icontrained'], function () {
			require(__DIR__.'/icontrained.php');
		});
		Route::group(['as' => 'message.',  'prefix' => 'message', 'namespace' => 'Message'], function () {
			require(__DIR__.'/message.php');
		});
		Route::group(['as' => 'work.',  'prefix' => 'work', 'namespace' => 'Work'], function () {
			require(__DIR__.'/work.php');
		});

		Route::group(['as' => 'category.',  'prefix' => 'category', 'namespace' => 'Category'], function () {
			require(__DIR__.'/category.php');
		});

		Route::group(['as' => 'gallery_category.',  'prefix' => 'gallery_category', 'namespace' => 'GalleryCategory'], function () {
			require(__DIR__.'/gallery_category.php');
		});

		Route::group(['as' => 'product.',  'prefix' => 'product', 'namespace' => 'Product'], function () {
			require(__DIR__.'/product.php');
		});

		Route::group(['as' => 'catelogs.',  'prefix' => 'catelogs', 'namespace' => 'Catelogs'], function () {
			require(__DIR__.'/catelogs.php');
		});
		Route::group(['as' => 'job.',  'prefix' => 'job', 'namespace' => 'Job'], function () {
			require(__DIR__.'/job.php');
		});
		Route::group(['as' => 'ourpartner.',  'prefix' => 'ourpartner', 'namespace' => 'Ourpartner'], function () {
			require(__DIR__.'/ourpartner.php');
		});
	

		

	
	});