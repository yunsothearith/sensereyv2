<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Testimonial

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'TestimonialController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'TestimonialController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TestimonialController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'TestimonialController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'TestimonialController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TestimonialController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'TestimonialController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'TestimonialController@updateStatus']);
});	