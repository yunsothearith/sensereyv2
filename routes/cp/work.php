<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Partner

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'WorkController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'WorkController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'WorkController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'WorkController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'WorkController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'WorkController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'WorkController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'WorkController@updateStatus']);
});	