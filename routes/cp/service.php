<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 						['as' => 'create', 			'uses' => 'ServiceController@create']);
	Route::get('/', 							['as' => 'index', 			'uses' => 'ServiceController@index']);
	Route::get('/edit/{id}', 					['as' => 'edit', 			'uses' => 'ServiceController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'ServiceController@update']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'ServiceController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'ServiceController@trash']);
	Route::post('order', 						['as' => 'order', 			'uses' => 'ServiceController@order']);
	Route::post('status', 						['as' => 'update-status', 	'uses' => 'ServiceController@updateStatus']);


	//Type
	Route::get('/{id}/list-gallery', 				        ['as' => 'list-gallery', 			'uses' => 'GalleryController@index']);
	Route::get('/{id}/create-gallery', 			        	['as' => 'create-gallery', 		    'uses' => 'GalleryController@create']);
	Route::put('/{id}/create-gallery', 				        ['as' => 'store-gallery', 			'uses' => 'GalleryController@store']);
	Route::get('/{id}/edit-gallery/{gallery_id}', 	    	['as' => 'edit-gallery', 			 'uses' => 'GalleryController@edit']);
	Route::post('/{id}/update-gallery/{gallery_id}', 	    ['as' => 'update-gallery', 			 'uses' => 'GalleryController@update']);
	Route::delete('/{id}/update-gallery', 				    ['as' => 'trash-gallery',            'uses' => 'GalleryController@trash']);
	
	//Service List
	Route::get('/{id}/list-service', 				        	['as' => 'list-service', 			'uses' => 'ServicelistController@index']);
	Route::get('/{id}/create-service-list', 			       	['as' => 'create-service-list', 	'uses' => 'ServicelistController@create']);
	Route::put('/{id}/create-service-list', 				    ['as' => 'store-service-list', 		'uses' => 'ServicelistController@store']);
	Route::get('/{id}/edit-service-list/{service_list_id}', 	['as' => 'edit-service-list', 		'uses' => 'ServicelistController@edit']);
	Route::post('/{id}/update-service-list/{service_list_id}', 	['as' => 'update-service-list',     'uses' => 'ServicelistController@update']);
	Route::delete('/{id}/update-service-list', 				    ['as' => 'trash-service-list',      'uses' => 'ServicelistController@trash']);
});	