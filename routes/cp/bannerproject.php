<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannerprojectController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannerprojectController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannerprojectController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannerprojectController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannerprojectController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannerprojectController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannerprojectController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannerprojectController@updateStatus']);
});	