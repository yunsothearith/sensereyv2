<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'TeamController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'TeamController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TeamController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'TeamController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'TeamController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TeamController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'TeamController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'TeamController@updateStatus']);
});	