<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Product

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ProductController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'ProductController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ProductController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ProductController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ProductController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ProductController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ProductController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ProductController@updateStatus']);

	Route::get('/content/{id}', 	['as' => 'content', 				'uses' => 'ProductController@content']);
	Route::post('/update-content', 	['as' => 'update-content', 			'uses' => 'ProductController@updateContent']);
	Route::get('/banner/{id}', 		['as' => 'banner', 					'uses' => 'ProductController@banner']);
	Route::post('/update-bannered', 	['as' => 'update-bannered', 			'uses' => 'ProductController@updateBanner']);

	Route::get('/logo/{id}', 			['as' => 'logo', 					'uses' => 'ProductController@logo']);
	Route::post('/update-logo', 		['as' => 'update-logo', 			'uses' => 'ProductController@updateLogo']);


	//Type
	Route::get('/{id}/list-type', 					['as' => 'list-type', 						'uses' => 'TypeController@index']);
	Route::get('/{id}/create-type', 				['as' => 'create-type', 					'uses' => 'TypeController@create']);
	Route::put('/{id}/create-type', 				['as' => 'store-type', 						'uses' => 'TypeController@store']);
	Route::get('/{id}/edit-type/{type_id}', 		['as' => 'edit-type', 						'uses' => 'TypeController@edit']);
	Route::post('/{id}/update-type/{type_id}', 		['as' => 'update-type', 					'uses' => 'TypeController@update']);
	Route::delete('/{id}/update-type', 				['as' => 'trash-type', 						'uses' => 'TypeController@trash']);
	Route::post('type-order', 						['as' => 'type-order', 						'uses' => 'TypeController@order']);


	//Item
	Route::get('/{id}/list-item', 					['as' => 'list-item', 						'uses' => 'ItemController@index']);
	Route::get('/{id}/create-item', 				['as' => 'create-item', 					'uses' => 'ItemController@create']);
	Route::put('/{id}/create-item', 				['as' => 'store-item', 						'uses' => 'ItemController@store']);
	Route::get('/{id}/edit-item/{item_id}', 		['as' => 'edit-item', 						'uses' => 'ItemController@edit']);
	Route::post('/{id}/update-item/{item_id}', 		['as' => 'update-item', 					'uses' => 'ItemController@update']);
	Route::delete('/{id}/update-item', 				['as' => 'trash-item', 						'uses' => 'ItemController@trash']);
	Route::post('list-item-order', 					['as' => 'list-item-order', 			'uses' => 'ItemController@order']);

	// Item Color
	Route::get('/{id}/item/{item_id}/list-color', 					['as' => 'list-color', 						'uses' => 'ColorController@index']);
	Route::get('/{id}/item/{item_id}/create-color',				    ['as' => 'create-color', 					'uses' => 'ColorController@create']);
	Route::put('/{id}/item/{item_id}/create-color', 				['as' => 'store-color', 					'uses' => 'ColorController@store']);
	Route::get('/{id}/item/{item_id}/edit-color/{color_id}', 		['as' => 'edit-color', 						'uses' => 'ColorController@edit']);
	Route::post('/{id}/item/{item_id}/update-color/{color_id}', 	['as' => 'update-color', 					'uses' => 'ColorController@update']);
	Route::delete('/{id}/color/', 									['as' => 'trash-color', 					'uses' => 'ColorController@trash']);
	Route::post('color-order', 										['as' => 'color-order', 					'uses' => 'ColorController@order']);
	// Item Color Size
	Route::get('/{id}/item/{item_id}/list-color/{item_color_id}/list-color-size', 								['as' => 'list-color-size', 					'uses' => 'ColorSizeController@index']);
	Route::get('/{id}/item/{item_id}/create-color/{item_color_id}/create-color-size',				    		['as' => 'create-color-size', 					'uses' => 'ColorSizeController@create']);
	Route::put('/{id}/item/{item_id}/create-color/{item_color_id}/store-color-size', 							['as' => 'store-color-size', 					'uses' => 'ColorSizeController@store']);
	Route::get('/{id}/item/{item_id}/edit-color/{item_color_id}/edit-color-size/{item_color_size_id}', 			['as' => 'edit-color-size', 					'uses' => 'ColorSizeController@edit']);
	Route::post('/{id}/item/{item_id}/update-color/{item_color_id}/update-color-size/{item_color_size_id}', 	['as' => 'update-color-size', 					'uses' => 'ColorSizeController@update']);
	Route::delete('/{id}/color-size/', 																			['as' => 'trash-color-size', 					'uses' => 'ColorSizeController@trash']);
	Route::post('color-size-order', 																			['as' => 'color-size-order', 					'uses' => 'ColorSizeController@order']);

	//Item Detail
	Route::get('/{id}/item/{item_id}/list-detail', 					['as' => 'list-detail', 					'uses' => 'DetailController@index']);
	Route::get('/{id}/item/{item_id}/create-detail',				['as' => 'create-detail', 					'uses' => 'DetailController@create']);
	Route::put('/{id}/item/{item_id}/create-detail', 				['as' => 'store-detail', 					'uses' => 'DetailController@store']);
	Route::get('/{id}/item/{item_id}/edit-detail/{detail_id}', 		['as' => 'edit-detail', 					'uses' => 'DetailController@edit']);
	Route::post('/{id}/item/{item_id}/update-detail/{detail_id}', 	['as' => 'update-detail', 					'uses' => 'DetailController@update']);
	Route::delete('/{id}/detail/', 									['as' => 'trash-detail', 					'uses' => 'DetailController@trash']);
	Route::post('detail-order', 									['as' => 'detail-order', 					'uses' => 'DetailController@order']);
	//Item Gallery
	Route::get('/{id}/item/{item_id}/list-gallery', 					['as' => 'list-gallery', 					'uses' => 'GalleryController@index']);
	Route::get('/{id}/item/{item_id}/create-gallery',					['as' => 'create-gallery', 					'uses' => 'GalleryController@create']);
	Route::put('/{id}/item/{item_id}/create-gallery', 					['as' => 'store-gallery', 					'uses' => 'GalleryController@store']);
	Route::get('/{id}/item/{item_id}/edit-gallery/{gallery_id}', 		['as' => 'edit-gallery', 					'uses' => 'GalleryController@edit']);
	Route::post('/{id}/item/{item_id}/update-gallery/{gallery_id}', 	['as' => 'update-gallery', 					'uses' => 'GalleryController@update']);
	Route::delete('/{id}/gallery/', 									['as' => 'trash-gallery', 					'uses' => 'GalleryController@trash']);
	Route::post('gallery-order', 										['as' => 'gallery-order', 					'uses' => 'GalleryController@order']);
	//Item Banner
	Route::get('/{id}/item/{item_id}/list-banner', 					['as' => 'list-banner', 					'uses' => 'BannerController@index']);
	Route::get('/{id}/item/{item_id}/create-banner',				['as' => 'create-banner', 					'uses' => 'BannerController@create']);
	Route::put('/{id}/item/{item_id}/create-banner', 				['as' => 'store-banner', 					'uses' => 'BannerController@store']);
	Route::get('/{id}/item/{item_id}/edit-banner/{banner_id}', 		['as' => 'edit-banner', 					'uses' => 'BannerController@edit']);
	Route::post('/{id}/item/{item_id}/update-banner/{banner_id}', 	['as' => 'update-banner', 					'uses' => 'BannerController@update']);
	Route::delete('/{id}/banner/', 									['as' => 'trash-banner', 					'uses' => 'BannerController@trash']);
	Route::post('banner-order', 									['as' => 'banner-order', 					'uses' => 'BannerController@order']);
});	