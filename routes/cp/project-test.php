<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Partner

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ProjectController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'ProjectController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ProjectController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ProjectController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ProjectController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ProjectController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ProjectController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ProjectController@updateStatus']);
});	

//Add images to package 
$PackageImageController = 'Project\ProjectImageController@';
Route::get('/{id}/images', 				            ['uses' => 'ProjectimageController@create']);
Route::get('/{id}/images', 			                ['uses' => 'ProjectimageController@edit']); 
Route::get('/{id}/images/{project_images_id}',    ['uses' => 'ProjectimageController@update']); 

