<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Projectprocess

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ProjectprocessController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'ProjectprocessController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ProjectprocessController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ProjectprocessController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ProjectprocessController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ProjectprocessController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ProjectprocessController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ProjectprocessController@updateStatus']);
});	