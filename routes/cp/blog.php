<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Blog

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BlogController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BlogController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BlogController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BlogController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BlogController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BlogController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BlogController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BlogController@updateStatus']);
});	