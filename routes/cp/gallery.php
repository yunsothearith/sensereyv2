<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Gallery

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'GalleryController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'GalleryController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'GalleryController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'GalleryController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'GalleryController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'GalleryController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'GalleryController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'GalleryController@updateStatus']);
});	