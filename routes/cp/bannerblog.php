<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannerblogController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannerblogController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannerblogController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannerblogController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannerblogController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannerblogController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannerblogController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannerblogController@updateStatus']);
});	