<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 						['as' => 'create', 			'uses' => 'CatelogsController@create']);
	Route::get('/', 							['as' => 'index', 			'uses' => 'CatelogsController@index']);
	Route::get('/edit/{id}', 					['as' => 'edit', 			'uses' => 'CatelogsController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'CatelogsController@update']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'CatelogsController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'CatelogsController@trash']);
	Route::post('order', 						['as' => 'order', 			'uses' => 'CatelogsController@order']);
	Route::post('status', 						['as' => 'update-status', 	'uses' => 'CatelogsController@updateStatus']);


	//Type
	Route::get('/{id}/list-gallery', 					['as' => 'list-gallery', 				  'uses' => 'GalleryController@index']);
	Route::get('/{id}/create-gallery', 				    ['as' => 'create-gallery', 				  'uses' => 'GalleryController@create']);
	Route::put('/{id}/create-gallery', 			     	['as' => 'store-gallery', 				  'uses' => 'GalleryController@store']);
	Route::get('/{id}/edit-gallery/{gallery_id}', 		['as' => 'edit-gallery', 				  'uses' => 'GalleryController@edit']);
	Route::post('/{id}/update-gallery/{gallery_id}',    ['as' => 'update-gallery', 				  'uses' => 'GalleryController@update']);
	Route::delete('/{id}/update-gallery', 				['as' => 'trash-gallery', 				  'uses' => 'GalleryController@trash']);
	
});	