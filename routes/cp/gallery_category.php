<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Gallerycategory

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'GalleryCategoryController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'GalleryCategoryController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'GalleryCategoryController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'GalleryCategoryController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'GalleryCategoryController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'GalleryCategoryController@trash']);

});	