<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannercontactController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannercontactController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannercontactController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannercontactController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannercontactController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannercontactController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannercontactController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannercontactController@updateStatus']);
});	