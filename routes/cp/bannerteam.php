<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bannerteam

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannerteamController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannerteamController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannerteamController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannerteamController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannerteamController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannerteamController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannerteamController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannerteamController@updateStatus']);
});	