<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Mission

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'MissionController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'MissionController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'MissionController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'MissionController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'MissionController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'MissionController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'MissionController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'MissionController@updateStatus']);
});	