<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Partner

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'OurpartnerController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'OurpartnerController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OurpartnerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'OurpartnerController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'OurpartnerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'OurpartnerController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'OurpartnerController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'OurpartnerController@updateStatus']);
});	