<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Certifiate

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CertifiateController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'CertifiateController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CertifiateController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CertifiateController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'CertifiateController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CertifiateController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'CertifiateController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'CertifiateController@updateStatus']);
});	