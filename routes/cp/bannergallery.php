<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Team

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannergalleryController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannergalleryController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannergalleryController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannergalleryController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannergalleryController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannergalleryController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannergalleryController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannergalleryController@updateStatus']);
});	