<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> category

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CategoryController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'CategoryController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CategoryController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CategoryController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'CategoryController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CategoryController@trash']);

});	