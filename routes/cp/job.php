<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Job

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'JobController@index']);
	Route::get('/create', 			['as' => 'createx', 		'uses' => 'JobController@create']);
	Route::put('/', 				['as' => 'storex', 			'uses' => 'JobController@store']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'JobController@edit']);
	Route::post('/', 				['as' => 'updatex', 		'uses' => 'JobController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'JobController@trash']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'JobController@updateStatus']);
	Route::get('/{id}/applicant', 	['as' => 'applicant', 		'uses' => 'JobController@applicants']);
	// Route::get('/{id}/applicant/create', 			['as' => 'create-applicant', 			'uses' => 'JobController@createApplicant']);
	// Route::put('/applicant', 						['as' => 'store-applicant', 			'uses' => 'JobController@storeApplicant']);
	// Route::get('/{id}/applicant/{applicant_id}', 	['as' => 'edit-applicant', 				'uses' => 'JobController@editApplicant']);
	// Route::post('/applicant', 						['as' => 'update-applicant', 			'uses' => 'JobController@updateApplicant']);
	// Route::delete('/applicant', 					['as' => 'trash-applicant', 			'uses' => 'JobController@trashApplicant']);
});