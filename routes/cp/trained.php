<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Blog

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'TrainedController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'TrainedController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TrainedController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'TrainedController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'TrainedController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TrainedController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'TrainedController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'TrainedController@updateStatus']);
});	