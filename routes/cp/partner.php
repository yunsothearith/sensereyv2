<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Partner

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'PartnerController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'PartnerController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PartnerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'PartnerController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'PartnerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PartnerController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'PartnerController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'PartnerController@updateStatus']);
});	