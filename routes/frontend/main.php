<?php
Route::get('{locale}/home',                                 ['as' => 'home',                              'uses' => 'HomeController@index']);
Route::get('{locale}/about',                                ['as' => 'about',                             'uses' => 'AboutController@index']);
Route::get('{locale}/aboutdetail',                          ['as' => 'aboutdetail',                       'uses' => 'AboutdetailController@index']);
Route::get('{locale}/services',                             ['as' => 'services',                          'uses' => 'ServicesController@index']);
Route::get('{locale}/project',                              ['as' => 'project',                           'uses' => 'ProjectController@index']);
Route::get('{locale}/project/{id}',                         ['as' => 'projectdetail',                     'uses' => 'ProjectController@detail']);
Route::get('{locale}/teams',                                ['as' => 'teams',                             'uses' => 'TeamsController@index']);
Route::get('{locale}/services/{id}',                        ['as' => 'servicedetail',                     'uses' => 'ServicesController@detail']);
Route::get('{locale}/blogs',                                ['as' => 'blogs',                             'uses' => 'BlogsController@index']);  
Route::get('{locale}/blogs/{id}',                           ['as' => 'blogdetail',                        'uses' => 'BlogsController@detail']); 
Route::get('{locale}/gallerys',                             ['as' => 'gallerys',                          'uses' => 'GallerysController@index']); 
Route::get('{locale}/career',                               ['as' => 'career',                            'uses' => 'CareerController@index']);
Route::get('{locale}/careerdetail',                         ['as' => 'careerdetail',                      'uses' => 'CareerdetailController@index']);
Route::get('{locale}/contact',                              ['as' => 'contact',                           'uses' => 'ContactController@index']);
Route::put('{locale}/submit-contact', 						[ 'as' => 'submit-contact',	                  'uses' => 'ContactController@store']);