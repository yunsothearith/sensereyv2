<?php

namespace App\Model\Item;
use Illuminate\Database\Eloquent\Model;

class ItemGalleries extends Model
{
   
    protected $table = 'item_galleries';
   
    public function item(){
        return $this->belongsTo('App\Model\Item\Item', 'item_id');
    }

    public function type(){
        return $this->belongsTo('App\Model\ProductType');
    }
    
}
