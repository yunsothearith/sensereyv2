<?php

namespace App\Model\Item;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
   
    protected $table = 'items';
   
    public function product(){
        return $this->belongsTo('App\Model\Product');
    }

    public function type(){
        return $this->belongsTo('App\Model\ProductType');
    }

    public function banners(){
        return $this->hasMany('App\Model\Item\ItemBanner', 'item_id');
    }
    public function colors(){
        return $this->hasMany('App\Model\Item\ItemColor', 'item_id');
    }
    public function details(){
        return $this->hasMany('App\Model\Item\ItemDetail', 'item_id');
    }
    public function galleries(){
        return $this->hasMany('App\Model\Item\ItemGalleries', 'item_id');
    }
}
