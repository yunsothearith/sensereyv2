<?php

namespace App\Model\Item;
use Illuminate\Database\Eloquent\Model;

class ItemColorSize extends Model
{
   
    protected $table = 'item_color_sizes';

    public function color(){
        return $this->belongsTo('App\Model\Item\ItemColor', 'color_id');
    }
}
