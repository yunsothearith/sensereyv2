<?php

namespace App\Model\Item;
use Illuminate\Database\Eloquent\Model;

class ItemDetail extends Model
{
    protected $table = 'item_details';
}
