<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class CatelogGalleries extends Model
{
   
    protected $table = 'catelog_galleries';

    public function catalog(){
        return $this->belongsTo('App\Model\Catalog', 'catelog_id');
    }
   
}
