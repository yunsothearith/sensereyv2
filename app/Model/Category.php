<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   
    protected $table = 'category';

    public function catelogs(){
        return $this->hasMany('App\Model\Catelogs', 'category_id');
    }
  
}
