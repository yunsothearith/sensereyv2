<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
   
    protected $table = 'gallery';
    public function category(){
        return $this->belongsTo('App\Model\GalleryCategory','gallery_category_id');
    }
  
}
