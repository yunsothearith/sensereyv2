<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Servicelist extends Model
{
    protected $table = 'service_List';
    public function service(){
        return $this->belongsTo('App\Model\service', 'service_id');
    }
   
}
