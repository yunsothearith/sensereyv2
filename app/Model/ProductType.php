<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
   
    protected $table = 'product_types';
    
    public function product(){
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function items(){
        return $this->hasMany('App\Model\Item\Item', 'type_id');
    }
}
