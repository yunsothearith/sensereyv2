<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   
    protected $table = 'products';
   
    public function items(){
        return $this->hasMany('App\Model\Item\Item', 'product_id');
    }
}
