<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class GalleryCategory extends Model
{
   
    protected $table = 'gallery_category';
    public function gallery(){
        return $this->hasMany('App\Model\Gallery','gallery_category_id');
    }
  
}
