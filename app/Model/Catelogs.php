<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Catelogs extends Model
{
   
    protected $table = 'catelogs';
   
    public function galleries(){
        return $this->hasMany('App\Model\CatelogGalleries', 'catelog_id');
    }

    public function product(){
        return $this->belongsTo('App\Model\Product');
    }

    public function category(){
        return $this->belongsTo('App\Model\Category','category_id');
    }
}
