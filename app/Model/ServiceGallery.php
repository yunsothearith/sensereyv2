<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ServiceGallery extends Model
{
   
    protected $table = 'service_gallery';

    public function service(){
        return $this->belongsTo('App\Model\service', 'service_id');
    }
   
}
