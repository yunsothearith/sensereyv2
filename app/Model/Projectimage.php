<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Projectimage extends Model
{
   
    use SoftDeletes;
    protected $table = 'project_images';

    public function project(){
        return $this->belongsTo('App\Model\Project', 'project_id');
    }
  
}
