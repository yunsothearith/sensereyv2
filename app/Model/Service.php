<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
   
    protected $table = 'service';
   
    public function gallery(){
        return $this->hasMany('App\Model\ServiceGallery', 'service_id');
    }
    public function servicelist(){
        return $this->hasMany('App\Model\ServiceList', 'service_id');
    }

    public function service(){
        return $this->belongsTo('App\Model\Service');
    }
}
