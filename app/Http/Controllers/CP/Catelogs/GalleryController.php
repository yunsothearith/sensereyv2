<?php

namespace App\Http\Controllers\CP\Catelogs;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\Catelogs as Model;

use App\Model\CatelogGalleries;


class GalleryController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.catelogs";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index ($id){
        $data = CatelogGalleries::select('*')->where('catelog_id', $id);
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data = $data->orderBy('data_order','ASC')->paginate($limit);
       
        return view($this->route.'.gallery.index', ['route'=>$this->route,'id'=> $id, 'data'=>$data,'appends'=>$appends]);
    }

    public function create( $id){
        return view($this->route.'.gallery.create' , ['route'=>$this->route,  'id'=>$id]);
    }
    public function store( $catelog_id ,Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'catelog_id'      => $catelog_id,
                    'creator_id'      => $user_id,
                    'catelog_id'      => $catelog_id,
                    'en_description'       =>      $request->input('en_description'),
                    'kh_description'       =>      $request->input('kh_description'),
                    'cn_description'       =>      $request->input('cn_description'),
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(),
                        [
                           
                            'image'         => [
                                               
                                                // Rule::dimensions()->width(900)->height(700),
                                            ],
                            
                        ]);
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/catelog/gallery');
        if($image != ""){
            $data['image'] = $image; 
        }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
      
		$id = CatelogGalleries::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit-gallery', ['id'=>$catelog_id, 'gallery_id'=>$id]));
    }

    public function edit( $catelog_id, $gallery_id){
        $data = CatelogGalleries::find($gallery_id);
        return view($this->route.'.gallery.edit' , ['route'=>$this->route, 'data'=>$data, 'id'=>$catelog_id , 'gallery_id'=>$gallery_id]);
    }

    public function update( $catelog_id, $gallery_id, Request $request){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'catelog_id'      => $catelog_id,
                    'en_description'       =>      $request->input('en_description'),
                    'kh_description'       =>      $request->input('kh_description'),
                    'cn_description'       =>      $request->input('cn_description'),
                    'updater_id'      => $user_id,
                    'updated_at'      => $now
                );
        

                Validator::make(
                    $request->all(),
			        	[
                            
                         
                            'image' => [
                                         
                                            // Rule::dimensions()->width(900)->height(700),
                            ],
                           
						]);
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/type');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        CatelogGalleries::where('id', $gallery_id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    
    public function trash($id){
        CatelogGalleries::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

}
