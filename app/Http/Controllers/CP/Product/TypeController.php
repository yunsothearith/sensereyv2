<?php

namespace App\Http\Controllers\CP\Product;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\Product as Model;

use App\Model\ProductType;


class TypeController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.product";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index ($id){
       
        $data = ProductType::select('*')->where('product_id', $id);
        
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('en_name', 'like', '%'.$key.'%')->orWhere('kh_name', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data = $data->orderBy('data_order','ASC')->paginate($limit);
       
        return view($this->route.'.type.index', ['route'=>$this->route, 'id'=> $id, 'data'=>$data,'appends'=>$appends]);
    }

    public function create($id){
        return view($this->route.'.type.create' , ['route'=>$this->route, 'id'=>$id]);
    }
    public function store($product_id ,Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'product_id'      => $product_id,
                    'kh_name'         => $request->input('kh_name'),
                    'en_name'         => $request->input('en_name'),
                    'kh_description'  => $request->input('kh_description'),
                    'en_description'  => $request->input('en_description'),
                    'creator_id'      => $user_id,
                    'created_at'      => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'kh_name'    => 'required',
                           'en_name'    => 'required',
                           'image'      => [
                                        'mimes:jpeg,png,jpg',
                                        ],
                            
                        ])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/type');
        if($image != ""){
            $data['image'] = $image; 
        }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
      
		$id = ProductType::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit-type', ['id'=>$product_id, 'type_id'=>$id]));
    }

    public function edit($product_id, $type_id){
        $data = ProductType::find($type_id);
        return view($this->route.'.type.edit' , ['route'=>$this->route, 'data'=>$data, 'id'=>$product_id , 'type_id'=>$type_id]);
    }

    public function update($product_id, $type_id, Request $request){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'product_id'      => $product_id,
                    'kh_name'         => $request->input('kh_name'),
                    'en_name'         => $request->input('en_name'),
                    'kh_description'  => $request->input('kh_description'),
                    'en_description'  => $request->input('en_description'),
                    'updater_id'      => $user_id,
                    'updated_at'      => $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'kh_name'    => 'required',
                            'en_name'    => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                           
						])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/type');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        ProductType::where('id', $type_id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    
    public function trash($id){
        ProductType::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }
    function order(Request $request){
        $string = $request->input('string');
        $data = json_decode($string);
        //print_r($data); die;
         foreach($data as $row){
            ProductType::where('id', $row->id)->update(['data_order'=>$row->order]);
         }
        return response()->json([
           'status' => 'success',
           'msg' => 'Data has been ordered.'
       ]);
     }

}
