<?php

namespace App\Http\Controllers\CP\Product;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\Product as Model;

use App\Model\Item\Item;

use App\Model\Item\ItemGalleries as ItemGallery;
use App\Model\ProductType as Type;

class GalleryController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.product";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index ($id, $item_id){
        $types = Type::where('product_id', $id)->get();
        $data = ItemGallery::select('*')->where('item_id', $item_id);
        
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key        =   isset($_GET['key'])?$_GET['key']:"";
        $type       =   isset($_GET['type'])?$_GET['type']:0;
        $from       =   isset($_GET['from'])?$_GET['from']:"";
        $till       =   isset($_GET['till'])?$_GET['till']:"";
        
        $appends    =   array('limit'=>$limit);
       
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $type       =   isset($_GET['type'])?$_GET['type']:0;
        if( $type > 0 ){
            $data = $data->where('type_id', $type);
            $appends['type'] = $type;
        }
        $data = $data->orderBy('data_order','ASC')->paginate($limit);
       
        return view($this->route.'.item.gallery.index', ['route'=>$this->route, 'id'=> $id, 'item_id'=> $item_id, 'data'=>$data,'appends'=>$appends, 'types'=>$types]);
    }

    public function create($id, $item_id){
        $types = Type::where('product_id', $id)->get();
        return view($this->route.'.item.gallery.create' , ['route'=>$this->route, 'id'=>$id, 'item_id'=>$item_id, 'types'=>$types]);
    }
    public function store($product_id, $item_id, Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'item_id'           => $item_id,
                    'code'              => $request->input('code'),
                    'kh_name'           => $request->input('kh_name'),
                    'en_name'           => $request->input('en_name'),
                    'kh_volume'         => $request->input('kh_volume'),
                    'en_volume'         => $request->input('en_volume'),

                    'kh_height'         => $request->input('kh_height'),
                    'en_height'         => $request->input('en_height'),
                    'kh_diameter'       => $request->input('kh_diameter'),
                    'en_diameter'       => $request->input('en_diameter'),

                    'kh_item_number'       => $request->input('kh_item_number'),
                    'en_item_number'       => $request->input('en_item_number'),

                    'kh_content'        => $request->input('kh_content'),
                    'en_content'        => $request->input('en_content'),

                    'type_id'           => $request->input('type_id'),

                    'creator_id'        => $user_id,
                    'created_at'        => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                           'image'      => [
                                       
                                        ],
                            
                        ])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/item/gallery');
        if($image != ""){
            $data['image'] = $image; 
        }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
      
		$id = ItemGallery::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit-gallery', ['id'=>$product_id, 'item_id'=>$item_id, 'gallery_id'=>$id]));
    }

    public function edit($product_id, $item_id, $gallery_id){
        $types = Type::where('product_id', $product_id)->get();
        $data = ItemGallery::find($gallery_id);
        return view($this->route.'.item.gallery.edit' , ['route'=>$this->route, 'data'=>$data, 'id'=>$product_id , 'item_id'=>$item_id, 'types'=>$types]);
    }

    public function update($product_id, $item_id, $gallery_id, Request $request){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
                    'item_id'           => $item_id,
                    'code'              => $request->input('code'),
                    'kh_name'           => $request->input('kh_name'),
                    'en_name'           => $request->input('en_name'),
                    'kh_volume'         => $request->input('kh_volume'),
                    'en_volume'         => $request->input('en_volume'),

                    'kh_height'         => $request->input('kh_height'),
                    'en_height'         => $request->input('en_height'),
                    'kh_diameter'       => $request->input('kh_diameter'),
                    'en_diameter'       => $request->input('en_diameter'),

                    'kh_item_number'       => $request->input('kh_item_number'),
                    'en_item_number'       => $request->input('en_item_number'),

                    'type_id'           => $request->input('type_id'),

                    'kh_content'        => $request->input('kh_content'),
                    'en_content'        => $request->input('en_content'),
                    'updater_id'        => $user_id,
                    'updated_at'        => $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            'image' => [
                                            
                            ],
                           
						])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/item/gallery');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        ItemGallery::where('id', $gallery_id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    
    public function trash($id){
       
        ItemGallery::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    function order(Request $request){
        $string = $request->input('string');
        $data = json_decode($string);
        //print_r($data); die;
         foreach($data as $row){
            ItemGallery::where('id', $row->id)->update(['data_order'=>$row->order]);
         }
        return response()->json([
           'status' => 'success',
           'msg' => 'Data has been ordered.'
       ]);
     }

}
