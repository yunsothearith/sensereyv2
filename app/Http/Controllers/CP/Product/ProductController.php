<?php

namespace App\Http\Controllers\CP\Product;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\Product as Model;

use App\Model\Item\Item;


class ProductController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.product";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key        =   isset($_GET['key'])?$_GET['key']:"";
        $from       =isset($_GET['from'])?$_GET['from']:"";
        $till       =isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('en_name', 'like', '%'.$key.'%')->orWhere('kh_name', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('data_order','ASC')->paginate($limit);
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data,'appends'=>$appends]);
    }
    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'kh_name'         => $request->input('kh_name'),
                    'en_name'         => $request->input('en_name'),
                    'kh_description'  => $request->input('kh_description'),
                    'en_description'  => $request->input('en_description'),
                    'creator_id' => $user_id,
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'kh_name'    => 'required',
                           'en_name'    => 'required',
                           'image'      => [
                                            'mimes:jpeg,png,jpg',
                            ],
                            
                        ])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product');
        if($image != ""){
            $data['image'] = $image; 
        }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
      
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }


    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'kh_name'         => $request->input('kh_name'),
                    'en_name'         => $request->input('en_name'),
                    'kh_description'  => $request->input('kh_description'),
                    'en_description'  => $request->input('en_description'),
                    'updater_id'    =>   $user_id,
                    'updated_at'    =>   $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'kh_name'    => 'required',
                            'en_name'    => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                           
						])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    

    public function content($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.content', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function updateContent(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'kh_content'         => $request->input('kh_content'),
                    'en_content'         => $request->input('en_content'),
                    'updater_id'    =>   $user_id,
                    'updated_at'    =>   $now
                );
    
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Content has been updated!' );
        return redirect()->back();
    }

    public function banner($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.banner', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function updateBanner(Request $request){
        
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'updater_id'    =>   $user_id,
                    'updated_at'    =>   $now
                );
        
        $banner = FileUpload::uploadFile($request, 'banner', 'uploads/product/banner');
        if($banner != ""){
            $data['banner'] = $banner; 
        }

        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Banner has been updated!' );
        return redirect()->back();
    }

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Published status has been updated.'
      ]);
    }

    public function logo($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.logo', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function updateLogo(Request $request){
        
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'updater_id'    =>   $user_id,
                    'updated_at'    =>   $now
                );
        
        $logo = FileUpload::uploadFile($request, 'logo', 'uploads/product/logo');
        if($logo != ""){
            $data['logo'] = $logo; 
        }

        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Logo has been updated!' );
        return redirect()->back();
    }

}
