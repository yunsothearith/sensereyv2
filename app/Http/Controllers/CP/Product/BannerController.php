<?php

namespace App\Http\Controllers\CP\Product;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\Product as Model;

use App\Model\Item\Item;

use App\Model\Item\ItemBanner;


class BannerController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.product";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index ($id, $item_id){
       
        $data = ItemBanner::select('*')->where('item_id', $item_id);
        
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('en_name', 'like', '%'.$key.'%')->orWhere('kh_name', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data = $data->orderBy('data_order','ASC')->paginate($limit);
       
        return view($this->route.'.item.banner.index', ['route'=>$this->route, 'id'=> $id, 'item_id'=> $item_id, 'data'=>$data,'appends'=>$appends]);
    }

    public function create($id, $item_id){
        return view($this->route.'.item.banner.create' , ['route'=>$this->route, 'id'=>$id, 'item_id'=>$item_id]);
    }
    public function store($product_id, $item_id, Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'item_id'           => $item_id,
                    'kh_name'         => $request->input('kh_name'),
                    'en_name'         => $request->input('en_name'),
                    'creator_id'      => $user_id,
                    'created_at'      => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'kh_name'    => 'required',
                           'en_name'    => 'required',
                           'image'      => [
                                        'mimes:jpeg,png,jpg',
                                        ],
                            
                        ])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/item/banner');
        if($image != ""){
            $data['image'] = $image; 
        }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
      
		$id = ItemBanner::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit-banner', ['id'=>$product_id, 'item_id'=>$item_id, 'banner_id'=>$id]));
    }

    public function edit($product_id, $item_id, $banner_id){
        $data = ItemBanner::find($banner_id);
        return view($this->route.'.item.banner.edit' , ['route'=>$this->route, 'data'=>$data, 'id'=>$product_id , 'item_id'=>$item_id]);
    }

    public function update($product_id, $item_id, $banner_id, Request $request){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'item_id'      => $item_id,
                    'kh_name'         => $request->input('kh_name'),
                    'en_name'         => $request->input('en_name'),
                    'updater_id'      => $user_id,
                    'updated_at'      => $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'kh_name'    => 'required',
                            'en_name'    => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                           
						])->validate();
        
        $image = FileUpload::uploadFile($request, 'image', 'uploads/product/item/banner');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        ItemBanner::where('id', $banner_id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    
    public function trash($id){
       
        ItemBanner::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    function order(Request $request){
        $string = $request->input('string');
        $data = json_decode($string);
        //print_r($data); die;
         foreach($data as $row){
            ItemBanner::where('id', $row->id)->update(['data_order'=>$row->order]);
         }
        return response()->json([
           'status' => 'success',
           'msg' => 'Data has been ordered.'
       ]);
     }

}
