<?php

namespace App\Http\Controllers\CP\Service;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\Service as Model;

use App\Model\ServiceGallery;
use App\Model\Servicelist;


class ServicelistController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.service";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index ($id){
        $data = Servicelist::select('*')->where('service_id', $id);
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $from=isset($_GET['from'])?$_GET['from']:"";
        $appends=array('limit'=>$limit);
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data = $data->orderBy('data_order','ASC')->paginate($limit);
        return view($this->route.'.service-list.index', ['route'=>$this->route,'id'=> $id, 'data'=>$data, 'appends'=>$appends]);
    }

    public function create( $id){
        return view($this->route.'.service-list.create' , ['route'=>$this->route,  'id'=>$id]);
    }
    public function store( $service_id ,Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'service_id'      => $service_id,
                    'creator_id'      => $user_id,
                    'service_id'      => $service_id,
                    'en_description' =>   $request->input('en_description'),
                   
                   
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(),
                        [
                          
                        

                            ])->validate();
         Validator::make(
         $request->all(),
                         [
                               'image'        => [
                                'mimes:jpeg,png,jpg',
                                Rule::dimensions()->width(900)->height(700),
                                                ],
                          ])->validate();
        
        // $image = FileUpload::uploadFile($request, 'image', 'uploads/service/gallery');
        // if($image != ""){
        //     $data['image'] = $image; 
        // }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
      
		$id = ServiceList::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
        // return redirect(route($this->route.'.service-list.edit', ['id'=>$service_id, 'service_list_id'=>$id]));
        // return view($this->route.'.edit' , ['route'=>$this->route,'data'=>$data , 'id'=>$service_id, 'service_list_id'=>$id]);
        return redirect(route($this->route.'.edit-service-list', ['id'=>$service_id, 'service_list_id'=>$id]));
    }
    public function edit( $service_id, $service_list_id){
        $data = ServiceList::find($service_list_id);
        return view($this->route.'.service-list.edit' , ['route'=>$this->route, 'data'=>$data, 'id'=>$service_id , 'service_list_id'=>$service_list_id]);
    }

    public function update( $service_id, $service_list_id, Request $request){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                            'service_id'      => $service_id,
                            'creator_id'      => $user_id,
                            'service_id'      => $service_id,
                            'en_description' =>   $request->input('en_description'),
                            'kh_description' =>   $request->input('kh_description'),
                            'cn_description' =>   $request->input('cn_description'),
                        );
        

                Validator::make(
                    $request->all(),
			        	[
                        ])->validate();

                 Validator::make(
                 $request->all(),
                                [
                                     'image'        => [
                                     'mimes:jpeg,png,jpg',
                                       Rule::dimensions()->width(900)->height(700),
                                                                   ],
                                ])->validate();
        
        // $image = FileUpload::uploadFile($request, 'image', 'uploads/product/type');
        // if($image != ""){
        //     $data['image'] = $image; 
        // }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        ServiceList::where('id', $service_list_id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    public function trash($id){
        ServiceList::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

}
