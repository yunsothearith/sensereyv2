<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Slide;
use App\Model\Service as Mains;
use App\Model\ServiceGallery;
use App\Model\Partner;
use App\Model\Trained;
use App\Model\Icontrained;
use App\Model\Blog;
use App\Model\Category;
use App\Model\Catelogs as Main;
use App\Model\CatelogGaleries;



class HomeController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $catelog = Main::select($locale.'_name as name','image','is_published','id', 'category_id')->with('category')->where(['is_published' => 1, 'is_progress' => 0])->limit(6)->get();
        $categories = Category::select($locale.'_title as title','id', 'slug')
        ->with(['catelogs'=> function ($q) use ($locale){
            $q->select($locale.'_name as name','image','is_published','id')->where('is_published',1);
        }])->get();


        $catelogProgress = Main::select($locale.'_name as name','image','is_published','id', 'category_id')->where(['is_published' => 1, 'is_progress' => 1])->limit(6)->get();


        $service = Mains::select($locale.'_name as name','image','id')->where('is_published',1)->get(); 
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        $partner = Partner::select('id','image')->where('is_published',1)->get();
        $blog = Blog::select('id',$locale.'_title as title','image',$locale.'_description as description')->where('is_published', 1)->orderBy('id', 'desc')->limit(3)->get();
        $trained = Trained::select($locale.'_title as title',$locale.'_description as description')->where('is_published',1)->get();
        $icontrained = Icontrained::select($locale.'_title as title','image')->where('is_published',1)->get();
        return view('frontend.home',
        [
            'locale'=>$locale,
             'defaultData' => $defaultData,
              'slide'=>$slide, 
              'partner'=>$partner, 
              'trained'=>$trained, 
              'icontrained'=>$icontrained, 
              'blog'=>$blog,
              'service'=>$service, 
              'catelog' =>$catelog, 
              'categories' => $categories,
              'catelogProgress' => $catelogProgress]);
  
 }
}