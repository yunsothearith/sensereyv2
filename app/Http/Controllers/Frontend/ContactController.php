<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


use App\Model\Message;
use App\Model\Bannercontact;
use App\Model\Slide;

use Session;
use Validator;

class ContactController extends FrontendController
{
    
    public function index($locale) {
        $defaultData = $this->defaultData($locale);
        $bannercontact = Bannercontact::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        return view('frontend.contact', ['locale'=> $locale, 'defaultData'=>$defaultData, 'bannercontact'=>$bannercontact,'slide'=>$slide]);
    }

    public function store(Request $request, $locale){
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
           
            'name'                      =>      $request->input('name'),
            'email'                     =>      $request->input('email'),
            'phone'                     =>      $request->input('phone'),
            'message'                   =>      $request->input('message'),
            'created_at'                =>      $now
        );
       

        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
            ], 

            [
                
            ])->validate();

        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        if($request->page == 'contact'){
            return redirect()->back();
        }else{
            return redirect($locale.'/home#sent-contact');
        }
        
    }
   
}