<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\Projectprocess;
use App\Model\Bannerproject;
use App\Model\Category;
use App\Model\Catelogs as Main;
use App\Model\CatelogGaleries;
use App\Model\Slide;

class ProjectController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $bannerproject = Bannerproject::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $catelog = Main::select($locale.'_name as name',$locale.'_service as service',$locale.'_location as location','size','image','is_published','id', 'category_id')->with('category')->where(['is_published' => 1, 'is_progress' => 0])->get();
        $categories = Category::select($locale.'_title as title','id', 'slug')
        ->with(['catelogs'=> function ($q) use ($locale){
            $q->select($locale.'_name as name','image','is_published','id')->where('is_published',1);
        }])->get();
        $catelogProgress = Main::select($locale.'_name as name','image','is_published','id', 'category_id')->where(['is_published' => 1, 'is_progress' => 1])->get();
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        return view('frontend.project',
        ['locale'=>$locale, 
        'defaultData' => $defaultData, 
        'bannerproject'=>$bannerproject,
         'catelog' =>$catelog,
         'categories' => $categories,
         'catelogProgress' => $catelogProgress,
         'slide'=>$slide,
           ]);
  
    }

    public function detail($locale = "en", $id = 0) {
        $defaultData = $this->defaultData($locale);
        $relateProduct = Main::select($locale.'_name as name',$locale.'_service as service',$locale.'_location as location','size','image','is_published','id', 'category_id')->orderBy('id','DESC')->limit(3)->get();
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        $data = Main::select($locale.'_name as name','image','is_published','id')->where(['is_published' => 1, 'id' => $id])->first();
        $bannerproject = Bannerproject::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $catelog = Main::select($locale.'_name as name',$locale.'_service as service','image','is_published','id', 'category_id',$locale.'_location as location','size')->with('category')->where(['id'=>$id])->first();
        return view('frontend.projectdetail',[
            'locale'=>$locale, 
            'defaultData' => $defaultData,
            'data' => $data, 
            'bannerproject'=>$bannerproject,
            'catelog' =>$catelog,
            'slide'=>$slide,
            'relateProduct'=>$relateProduct,
            ]);
  
    }
}