<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Bannerservice;

class ServicedetailController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $bannerservice = Bannerservice::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        return view('frontend.servicedetail',['locale'=>$locale, 'defaultData' => $defaultData, 'bannerservice' => $bannerservice]);
    }
}