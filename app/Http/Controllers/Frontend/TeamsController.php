<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Team;
use App\Model\Bannerteam;

class TeamsController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $team = Team::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $bannerteam = Bannerteam::select($locale.'_title as title','image',$locale.'_description as description',$locale.'_paragrab as paragrab')->where('is_published',1)->get();
        return view('frontend.teams',['locale'=>$locale, 'defaultData' => $defaultData, 'team'=>$team, 'bannerteam'=>$bannerteam]);
  
 }
}