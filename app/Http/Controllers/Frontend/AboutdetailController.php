<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Slide;


class AboutdetailController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        return view('frontend.aboutdetail',
        ['locale'=>$locale, 
        'defaultData' => $defaultData,
        'slide'=>$slide]);
  
 }
}