<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Persident;
use App\Model\History;
use App\Model\Vision;
use App\Model\Mission;
use App\Model\Certifiate;
use App\Model\Bannerabout;
use App\Model\Work;
use App\Model\Slide;
use App\Model\Ourpartner;



class AboutController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $ourpartner = Ourpartner::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        $work = Work::select($locale.'_title as title',$locale.'_description as description')->where('is_published',1)->get();
        $bannerabout = Bannerabout::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $history = History::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $persident = Persident::select($locale.'_title as title',$locale.'_role as role','image',$locale.'_description as description')->where('is_published',1)->get();
        $vision = Vision::select($locale.'_title as title',$locale.'_description as description')->where('is_published',1)->get();
        $mission = Mission::select($locale.'_title as title',$locale.'_description as description')->where('is_published',1)->get();
        $certifiate = Certifiate::select('id','image')->where('is_published',1)->get();
        return view('frontend.about',
        ['locale'=>$locale, 
        'defaultData' => $defaultData, 
        'history'=>$history,
        'work'=>$work, 
        'persident'=>$persident,
        'vision'=>$vision,
        'mission'=>$mission,
        'certifiate'=>$certifiate,
        'bannerabout'=>$bannerabout,
        'slide'=>$slide,
        'ourpartner'=>$ourpartner,
             ]);
  
 }
}