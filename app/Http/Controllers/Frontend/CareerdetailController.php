<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Bannercareer;
use App\Model\Job;
use App\Model\Slide;

class CareerdetailController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $bannercareer = Bannercareer::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        $job = Job::select($locale.'_title as title','salary','deadline',$locale.'_description as description',$locale.'_location as location')->where('is_published',1)->get();
        return view('frontend.careerdetail',
        ['locale'=>$locale, 
        'defaultData' => $defaultData,
         'bannercareer' => $bannercareer,
          'job' => $job,
          'slide'=>$slide]);
 }
}