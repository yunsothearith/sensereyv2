<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Gallery;
use App\Model\GalleryCategory;
use App\Model\Bannergallery;
use App\Model\Slide;

class GallerysController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        $gallery = Gallery::select('id','image', 'gallery_category_id')->where('is_published',1)->get();
        $galleryCategory = GalleryCategory::select($locale.'_title as title','id', 'slug')->get();
        $bannergallery = Bannergallery::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        return view('frontend.gallerys',
        ['locale'=>$locale, 
        'defaultData' => $defaultData,
         'gallery'=>$gallery,
          'bannergallery'=>$bannergallery, 
          'galleryCategory' => $galleryCategory,
          'slide'=>$slide]);
  
 }
}