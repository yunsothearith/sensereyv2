<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\Blog as Blog;
use App\Model\Bannerblog;


class BlogsController extends FrontendController
{
    
public function index($locale = "en") {
    $defaultData = $this->defaultData($locale);
    $bannerblog = Bannerblog::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
    $blog = Blog::select('id',$locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
    return view('frontend.blogs',['locale'=>$locale, 'defaultData' => $defaultData, 'blog'=>$blog, 'bannerblog'=>$bannerblog]);
  
 }
 
 public function detail($locale = "en",$id=0) {
    $defaultData = $this->defaultData($locale);
    $blog = Blog::select('id',$locale.'_title as title','image',$locale.'_description as description')->where(['id'=>$id])->first();
    $relateblog = Blog::select('id',$locale.'_title as title','image',$locale.'_description as description')->orderBy('id','DESC')->limit(7)->get();
    $bannerblog = Bannerblog::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
    return view('frontend.blogsdetail',['locale'=>$locale, 'defaultData' => $defaultData, 'blog'=>$blog, 'bannerblog'=>$bannerblog, 'relateblog'=>$relateblog]);
 }

}