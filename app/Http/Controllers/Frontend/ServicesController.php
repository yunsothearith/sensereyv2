<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


use App\Model\Bannerservice;
use App\Model\Service as Mains;
// use App\Model\ServiceGallery;
use App\Model\ServiceList;
use App\Model\Slide;

class ServicesController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
        $service = Mains::select($locale.'_name as name',$locale.'_description as description','image','id')->where('is_published',1)->get();
        $bannerservice = Bannerservice::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        return view('frontend.services',
        ['locale'=>$locale,
         'defaultData' => $defaultData,
          'service'=>$service,
           'bannerservice'=>$bannerservice,
           'slide'=>$slide]);
  
 }


     public function detail($locale = "en", $id = 0) {
    
    $defaultData = $this->defaultData($locale);
    $slide = Slide::select($locale.'_title as title','image',$locale.'_description as description', 'is_position')->where('is_published',1)->get();
    $datas = Mains::select($locale.'_name as name',$locale.'_description as description','image','is_published','id')->where(['is_published' => 1, 'id' => $id])->first();
    $service = Mains::select($locale.'_name as name','image','is_published','id')->orderBy('id','DESC')->where('is_published',1)->get();
    $bannerservice = Bannerservice::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
    // $servicelist = ServiceList::select('id', $locale.'_description as description','service_id')->where('is_published',1)->get();
    // $servicelist = servicelist::select($locale.'_description as description','is_published','id','service_id')->where(['is_published' => 1, 'id' => $id])->get();
    $servicelist = ServiceList::select($locale.'_description as description','is_published','id', 'service_id')->where(['is_published' => 1, 'service_id' => $id])->get();
    return view('frontend.servicedetail',
    ['locale'=>$locale, 
    'defaultData' => $defaultData,
     'bannerservice' => $bannerservice, 
     'datas' => $datas, 
     'service'=>$service,
     'servicelist'=>$servicelist,
     'slide'=>$slide]);

}

}