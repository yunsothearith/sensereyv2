@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-blogs', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

<!-- Start Content -->
<section class="content">

	@include('frontend/side.slide', ['slide' => $slide])

	<!-- Start Breadcrumbs -->
	{{-- <div class="breadcrumbs_outer">
	  <div class="container">
		<ul class="breadcrumbs">
		  <li><a href="index.html">Home</a> > </li>
		  <li>Blog</li>
		</ul>
	  </div>
	</div> --}}
	<!-- End Breadcrumbs -->


	<!-- Start Blogs sec -->
<div class="service-outer blog-outer blox-box">
    <div class="container">
      <div class="service-list">
        <div class="row">
            @if(Count($blog) >0)
			@foreach( $blog as $row)
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="service-box">
				<figure><a class="nav-link" href="{{route('blogdetail',['locale'=>$locale, 'id' => $row->id])}}"><img src="{{ asset ($row->image)}}" alt=""></a></figure>
				<div class="service-detail">
					<a class="nav-link" href="{{route('blogdetail',['locale'=>$locale, 'id' => $row->id])}}"><h4 style="margin-top: -33px;">{{$row->title}}</h4></a>
                    <a class="nav-link" href="{{route('blogdetail',['locale'=>$locale, 'id' => $row->id])}}"><p style="color:black;    margin-top: -14px;">{{$row->description}}</p></a>
                    <a href="{{route('blogdetail',['locale'=>$locale, 'id' => $row->id])}}" class="btn">Read More</a> 
				</div>
			  </div>
          </div>
          @endforeach
          @else
          <div class="col-12">
              <div class="alert alert-warning" role="alert">
                  គ្មាន​ទិន្នន័យ
              </div>
          </div>
      @endif
        </div>
      </div>
    </div>
  </div>
  <!-- End Blogs sec --> 

</section>
<!-- End Content sec --> 
@endsection