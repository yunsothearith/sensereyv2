@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-blogs', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

<!-- Start Content -->
<section class="content">

	@include('frontend/side.slide', ['slide' => $slide])

	<!-- Start Breadcrumbs -->
	<div class="breadcrumbs_outer">
	  <div class="container">
		<ul class="breadcrumbs">
		  <li><a href="index.html">Home</a> > </li>
		  <li><a href="blog.html">Blog</a> > </li>
		  <li>Blog Detail</li>
		</ul>
	  </div>
	</div>
	<!-- End Breadcrumbs --> 

	<!-- Start Blog Detail -->
	<div class="container blog-wrapper">
	  <div class="row"> 
		<!-- Start Left Column -->
		<div class="col-sm-8 col-md-9 blog-left">
		  <article class="blog-listing detail">
			<figure class="p-t-25 clearfix"><img src="{{ asset ($blog->image)}}"  style="width: 792px;"></figure>
			  <p> {!!$blog->title ?? ''!!}</p>
			  <p> {!!$blog->description ?? ''!!}</p>
			</li>
		  </article>
		</div>
		<!-- End Left Column --> 
		
		<!-- Start Right Column -->
		<div class="col-sm-4 col-md-3 blog-right">
		  {{-- <div style=" margin-top: 21px;"class="search-block clearfix">
			<input name="Search" type="text" placeholder="Search">
			<button class="search"><span class="icon-search-icon"></span></button>
		  </div> --}}
		  {{-- <div class="category">
			<h3>Categories</h3>
			<ul>
			  <li><a href="#"><span class="fa fa-long-arrow-right"></span> Busivness </a></li>
			  <li><a href="#"><span class="fa fa-long-arrow-right"></span> Construction </a></li>
			  <li><a href="#"><span class="fa fa-long-arrow-right"></span> Design </a></li>
			  <li><a href="#"><span class="fa fa-long-arrow-right"></span> Interiors </a></li>
			  <li><a href="#"><span class="fa fa-long-arrow-right"></span> Startups </a></li>
			</ul>
		  </div> --}}
		  <div class="recent-post">
			<h3>Recent Blog</h3>
			<ul>
			  <li class="clearfix"> <a href="#">
				@foreach( $relateblog as $row)
				<div class="img-block"><img src="{{ asset ($row->image)}}" class="img-responsive" alt=""></div>
				<div class="detail">
					<a  href="{{route('blogdetail',['locale'=>$locale, 'id' => $row->id])}}"><h4>{{$row->title}}</h4></a>
				  <p>June 18, 2020</p>
				</div>
				</a> 
				@endforeach
			</li>
			</ul>
		  </div>
		</div>
		<!-- End Right Column --> 
	  </div>
	</div>
	<!-- End Blog Detail --> 

</section>
<!-- End Content sec --> 

@endsection