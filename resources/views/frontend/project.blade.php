@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-project', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')


<!-- Start Content -->
<section class="content">

	@include('frontend/side.slide', ['slide' => $slide])
	
     <!-- Start feature sec -->
	 <div class="feature-outer">
		<div class="container">
		  <div class="head">
			<h3>{{__('general.project-progress')}}</h3>
			<p>{{__('general.company-is-involved-in-every-aspect -of-a-construction-and-experienced.')}}</p>
		  </div>
		  <div class="my">
			<ul class="tabs">
			  <li class="active"><a href="#">{{__('general.all-group')}}</a></li>
			  @foreach($categories as $row)
				<li class=""><a href="#" data-filter=".{{$row->slug}}-1">{{$row->title}}</a></li>
			  @endforeach
			</ul>
		  </div>
		  <div class="feature-list" id="freewall-1">
			<div class="row tab_container">
			  @foreach($catelogProgress as $row)
					  <div class="item {{$row->category->slug ?? ''}}-1">
						<div class="feature-box">
						  <figure><a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}"><img src="{{ asset($row->image) }}" alt=""></a></figure>
						  <div class="thumb-overlay">
							<div class="thumb-overlay-inner"> <a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}">
							<h5>{{$row->name}}</h5>
							  <p>{{$row->location}}</p>
							  </a> </div>
						  </div>
						</div>
					  </div>
				@endforeach
			</div>
		  </div>
		</div>
	  </div>
	  <!-- End feature sec --> 

	<!-- Start feature sec -->
	    <!-- Start feature sec -->
		<div class="feature-outer">
			<div class="container">
			  <div class="head">
				<h3>{{__('general.complet-project')}}</h3>
				<p>{{__('general.company-is-involved-in-every-aspect-of-a-construction-and-experienced')}}</p>
			  </div>
			      <ul class="tabs">
				<li class="active"><a href="#">{{__('general.all-group')}}</a></li>
				@foreach($categories as $row)
				    <li class=""><a href="#" data-filter=".{{$row->slug}}">{{$row->title}}</a></li>
				@endforeach
		    	  </ul>
			  <div class="feature-list" id="freewall">
				<div class="row tab_container">
		    @foreach($catelog as $row)
				  {{-- @php(dd($row->category->slug)) --}}
					<div class="item {{$row->category->slug ?? ''}}">
					  <div class="feature-box">
						<figure><a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}"><img src="{{ asset($row->image) }}" alt=""></a></figure>
						<div class="thumb-overlay">
						<div class="thumb-overlay-inner"> <a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}">
						<h5>{{$row->name}}</h5>
						  <p>Location :KOI Thé WB Arena, 2, NR2, Phnom Penh</p>
						  </a> </div>
						</div>
					  </div>
					</div>
			@endforeach
				</div>
			  </div>
			</div>
		  </div>
		  <!-- End feature sec --> 
	<!-- End feature sec --> 

</section>
<!-- End Content sec --> 


@endsection