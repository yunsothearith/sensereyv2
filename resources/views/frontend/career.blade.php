@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-career', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')
@include('frontend/side.slide', ['slide' => $slide])
	<!-- Start offer sec -->
	<div class="offer-outer">
		<div class="container">
		  <div class="row">
			<div class="col-sm-6 col-xs-12">
			  <h3>{{__('general.Graphic-Designer-Internship')}}<br/>
			  <p>{{__('general.We are looking for Graphic Designer Internship. Please read the job announcement below for your information.')}}</p>
			  {{-- <p>Published date: May 1, 2020Close date: May 31, 2020</p> --}}
			  <a href="{{route('careerdetail',['locale'=>$locale])}}" class="btn">{{__('general.read-more')}}</a> </div>
		
		  </div>
		</div>
	  </div>
	  <!-- End Offer sec --> 
@endsection