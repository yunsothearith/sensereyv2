@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-service', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')


<!-- Start Content -->
<section class="content">

	<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
		{{-- <ol class="carousel-indicators">
		  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
		  <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
		  <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
		  <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
		</ol> --}}
		<div class="carousel-inner">
		  @php($i = 1)
		  @foreach( $slide as $row)
		  <div class="carousel-item  @if($i++ == 1) active @endif" >
			<img src="{{ asset ($row->image)}}" class="d-block w-100" alt="">
			<div class="carousel-caption d-none d-md-block">
			 <h1 class=" @if($row->is_position == 1) text-slid-right @else text-slid-left @endif">{{ $row->title }}</h1>
			</div>
		  </div>
		  @endforeach
		</div>
		<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
		  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		  <span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
		  <span class="carousel-control-next-icon" aria-hidden="true"></span>
		  <span class="sr-only">Next</span>
		</a>
	  </div>


	<!-- Start Service Detail -->
	<div class="container service-wrapper">
	  <div class="row"> 
		<!-- Start Left Column -->
		<div class="col-sm-4">
		  <div class="service-left">
		  	<h3>{{$datas->name}}</h3>
			@foreach($servicelist as $row)
				<ul class="service-list">
					<li class="active"><a href="#"><span></span>{{ $row->description }}</a></li>
				</ul>
			@endforeach
			<br/>
		  </div>
		</div>
		<!-- End Left Column --> 
		
		<!-- Start Right Column -->
		<div class="col-sm-8 service-right">
		  
			<div id="thumb_slider" class="thumb_slider owl-carousel">
				@foreach ($datas->gallery as $row )
					<div class="item"><figure><img src="{{ asset($row->image) }}" alt="" class="mySlides"></figure></div>
				@endforeach
			</div>
		  
			<ul id="service-thumbnil" class="service-thumbnil clearfix owl-carousel">
				@foreach ($datas->gallery as $row )
					<li><span><img src="{{ asset($row->image) }}" alt=""></span></li>
				@endforeach
			</ul>
		  
		  <p>{!!$datas->description ?? ''!!}</p>
		 
		</div>
		<!-- End Right Column --> 
	  </div>
	</div>
	<!-- End Service Detail --> 
</section>
<!-- End Content sec --> 


@endsection