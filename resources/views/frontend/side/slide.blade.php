<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    {{-- <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
    </ol> --}}
    <div class="carousel-inner">
      @php($i = 1)
      @foreach( $slide as $row)
      <div class="carousel-item  @if($i++ == 1) active @endif" >
        <img src="{{ asset ($row->image)}}" class="d-block w-100" alt="">
        <div class="carousel-caption d-none d-md-block">
         <h1 class=" @if($row->is_position == 1) text-slid-right @else text-slid-left @endif">{{ $row->title }}</h1>
        </div>
      </div>
      @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <i class="fa fa-chevron-right" aria-hidden="true"></i>
    </a>
  </div>