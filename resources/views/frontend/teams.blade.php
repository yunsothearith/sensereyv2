@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-teams', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')
<!-- Start Banner -->
<div class="banner-outer inner-banner"> <span class="banner-shadow"></span>
	<div class="banner-image team-banner-image">
		@foreach( $bannerteam as $row)
		<div class="content animated fadeIn">
			<h1 class="animated fadeIn">{{$row->title}}</h1>
			<p class="animated fadeIn">{{$row->description}}</p>
		  </div>
			<img src="{{ asset ($row->image)}}" alt style="margin-top: -760px;;width: 1905px; height: 637px;">
		@endforeach
	</div>
</div>
<!-- End Banner --> 


	<!-- Start Team -->
	<div style="margin-top: -86px;"class="team-outer gray-bg">
	  <div class="container">
		<div class="head">
			@foreach( $bannerteam as $row)
		  <h3>Our Team</h3>
		  <p>{{$row->paragrab}}</p>
		   @endforeach
		</div>
		<div class="team-list">
		  <div class="row">
			@if(Count($team) >0) 
			@foreach( $team as $row)
			<div class="col-sm-3 col-xs-12">
			
			  <div class="team-box">
				<figure><img src="{{ asset ($row->image)}}" alt=""></figure>
				<h4>Yoxi Clark</h4>
				<p>Architecture Designer</p>
			  </div>
			
			</div>
			@endforeach
			@else
			<div class="col-12">
				<div class="alert alert-warning" role="alert">
					គ្មាន​ទិន្នន័យ
				</div>
			</div>
		@endif
		  </div>
		</div>
	  </div>
	</div>
	<!-- End Team sec --> 
@endsection