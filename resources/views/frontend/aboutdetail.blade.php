@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-about', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

<!-- Start Content -->
<section class="content">

	<!-- Start Banner -->
	<div class="banner-outer inner-banner"> <span class="banner-shadow"></span>
	  <div class="banner-image about-banner-image">
		<div class="container">
		  <div class="content animated fadeIn">
			<h1 class="animated fadeIn">About Detail</h1>
			<p class="animated fadeIn">SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company.</p>
		  </div>
		</div>
	  </div>
	</div>
	<!-- End Banner --> 

	<!-- Start Breadcrumbs -->
	<div class="breadcrumbs_outer">
	  <div class="container">
		<ul class="breadcrumbs">
		  <li><a href="index.html">Home</a> > </li>
		  <li>About Us</li> > <li>About Detail</li>
		</ul>
	  </div>
	</div>
	<!-- End Breadcrumbs --> 

	<!-- Start About Detail -->
	<div class="about-detail-outer">
	  <div class="container">
		<div class="row">
		  <div class="col-sm-12 col-xs-12">
			<div style="margin-bottom:-32px; margin-top: 28px;"class="imp-quote">
			<p>Today, Cambodia society is in the midst of dramatic, fast-paced change.
				Customer needs for buildings and other structures continue to change and diversify.
				These needs include responses to provide the right people, systems and
				technologies, and contributions to customer business activities. SENSEREY seeks
				to increase customer satisfaction and contribute to society by providing value that
				surpasses all expectations. Each structure SENSEREY builds incorporates ideas
				that are drawn from a wide range of parties while centring on the needs of our
				customers. SENSEREY identifies these ideas and applies them in a way that
				breathes new life into its structures.By applying such passion to each and every project, SENSEREY is ensuring
				that generations to come can be proud of the work we do today.
				</p>
		</div>
    	</div>
		</div>
	  </div>
	</div>
	<!-- End About Detail sec --> 
</section>
<!-- End Content sec --> 


@endsection