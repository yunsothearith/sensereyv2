@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-project', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

<!-- Start Content -->
<section class="content">

	@include('frontend/side.slide', ['slide' => $slide])

	<!-- Start Paragraph sec -->
	<div class="project-top">
	  <div class="container">
		<div class="row">
		  <div class="col-sm-8 col-xs-12">

			<div id="thumb_slider" class="thumb_slider owl-carousel">
				@foreach ($data->galleries as $row )
					<div class="item"><figure><img src="{{ asset($row->image) }}" alt="" class="mySlides"></figure></div>
				@endforeach
			  {{-- <div class="item"><figure><img src="{{ asset('public/frontend/images/banner/sensereybanner.png') }}" alt="" class="mySlides"></figure></div>
			  <div class="item"><figure><img src="{{ asset('public/frontend/images/banner/logh.png') }}" alt="" class="mySlides"></figure></div>
			  <div class="item"><figure><img src="{{ asset('public/frontend/images/banner/sensereybanner.png') }}" alt="" class="mySlides"></figure></div>
			  <div class="item"><figure><img src="{{ asset('public/frontend/images/banner/banner.jpg') }}" alt="" class="mySlides"></figure></div>
			  <div class="item"><figure><img src="{{ asset('public/frontend/images/banner/logh.png') }}" alt="" class="mySlides"></figure></div> --}}
			</div>
			
			<ul id="service-thumbnil" class="service-thumbnil clearfix owl-carousel">
				@foreach ($data->galleries as $row )
					<li><span><img src="{{ asset($row->image) }}" alt=""></span></li>
				@endforeach
			  
			  {{-- <li><span><img src="{{ asset('public/frontend/images/banner/sensereybanner.png') }}" alt=""></span></li>
			  <li><span><img src="{{ asset('public/frontend/images/banner/logh.png') }}" alt=""></span></li>
			  <li><span><img src="{{ asset('public/frontend/images/banner/sensereybanner.png') }}" alt=""></span></li>
			  <li><span><img src="{{ asset('public/frontend/images/banner/banner.jpg') }}" alt=""></span></li>
			  <li><span><img src="{{ asset('public/frontend/images/banner/logh.png') }}" alt=""></span></li> --}}
			</ul>
		  </div>
	    <div class=" project-info1 col-sm-4 col-xs-12">
			<div class="imp-quote">
			  <h3>Project Information</h3>
			  <ul class="project-info-list">
				<li>{!!$catelog->name ?? ''!!}</li>
				<li>{!!$catelog->location ?? ''!!}</li>
				<li>Duration:12 Months</li>
				<li>{!!$catelog->service ?? ''!!}</li>
				<li>{!!$catelog->size ?? ''!!}</li>
			  </ul>
			</div>
     	</div>
		</div>
	  </div>
	</div>
	<!-- End Paragraph sec --> 
	<!-- Start similar-project sec -->
	<div style="margin-top: 16px;"class="similar-project gray-bg">
	  <div class="container">
		<h4 >Featured Projects</h4>
		<div class="feature-list">
		  <div class="row">
			@foreach ($relateProduct as $row )
			<div class="col-sm-4 col-xs-12">
			  <div class="feature-box">
				<figure><a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}"><img src="{{ asset($row->image) }}" alt=""></a></figure>
				<div class="thumb-overlay">
				  <div class="thumb-overlay-inner"> <a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}">
					<h5>VILLA CLASSIC</h5>
					<p>Location : Monivong Blvd., Sangkat Sras Chork,Khan Doun Penh, Phnom Penh.</p>
					</a> </div>
				</div>
			  </div>
			</div>
			@endforeach
		  </div>
		</div>
	  </div>
	</div>
	<!-- End similar-project sec --> 

</section>
<!-- End Content sec --> 


@endsection