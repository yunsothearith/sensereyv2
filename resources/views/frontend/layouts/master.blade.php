<!doctype html>
<html lang="en">

<!-- Mirrored from webdevproof.com/theme-forest-demo/onena/demo-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 May 2020 15:49:31 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/frontend/images/khn.png') }}">
<title>Senserey Website</title>
<!-- Reset CSS -->
<link href="{{ asset('public/frontend/css/reset.css') }}" rel="stylesheet" type="text/css">
<!-- Bootstrap -->
<link href="{{ asset('public/frontend/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<!-- Font Awesome -->
<link href="{{ asset('public/frontend/assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<!-- Iconmoon -->
<link href="{{ asset('public/frontend/assets/iconmoon/css/iconmoon.css') }}" rel="stylesheet" type="text/css">
<!-- Owl Carousel -->
<link href="{{ asset('public/frontend/assets/owl-carousel/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
<!-- Animate -->
<link href="{{ asset('public/frontend/css/animate.css') }}" rel="stylesheet" type="text/css">
<!-- Font Css Style -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
<!-- Custom Style -->
<link href="{{ asset('public/frontend/css/custom.css') }}" rel="stylesheet" type="text/css">
<!-- Favicon -->
<link rel="icon" href="{{ asset('public/frontend/images/logopnh.png') }}" type="image/png" />


<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
</head>
<body>

<!-- Start Header -->
<header> 
  <!-- Start Header Sec -->
  <div class="container header-sec">
    <div class="row"> 
      <div class="col-xs-12 col-sm-3 logo">
        <div class="navbar-header image">
          <a href="{{route('home',['locale'=>$locale])}}"><img src="{{ asset('public/frontend/images/khn.png') }}" class="img-responsive" alt=""></a>
        </div>
      </div>
      <div class="col-xs-12 col-sm-9 header-right-bottom"> 
        <!-- Start Header Right Top -->
        <div class="header-right-top"> <a class="tel-number" href="tel:+855 15 855 558"><i class="fa fa-phone" aria-hidden="true"></i> +855 15 855 558</a> <a class="email-info" href="{{route('contact',['locale'=>$locale])}}"><i class="fa fa-envelope" aria-hidden="true"></i>lysenserey@senserey.com</a>
          <a href="{{route($defaultData['routeName'], $defaultData['enRouteParamenters'])}}"><img class="flet-icon" src="{{ asset('public/frontend/images/logo/hgf.png') }}" alt=""></a>
          <a href="{{route($defaultData['routeName'], $defaultData['khRouteParamenters'])}}"><img class="flet-icon" src="{{ asset('public/frontend/images/logo/bng.png') }}" alt=""></a>
          <a href="{{route($defaultData['routeName'], $defaultData['cnRouteParamenters'])}}"><img class="flet-icon" src="{{ asset('public/frontend/images/logo/nhgfdd.png') }}" alt=""></a>
        </div>
        <!-- End Header Right Top --> 
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-md navbar-dark navbar-custom">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"> <span class="navbar-toggler-icon"></span> </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item  @yield('active-home')"> <a class="nav-link" href="{{route('home',['locale'=>$locale])}}">{{__('general.home')}}</a> </li>
              <li class="nav-item @yield('active-about')"> <a class="nav-link" href="{{route('about',['locale'=>$locale])}}">{{__('general.about')}}</a> </li>
              <li class="nav-item @yield('active-service')"> <a class="nav-link" href="{{route('services',['locale'=>$locale])}}">{{__('general.service')}}</a> </li>
              <li class="nav-item @yield('active-project')"> <a class="nav-link" href="{{route('project',['locale'=>$locale])}}">{{__('general.project')}}</a> </li>
              {{-- <li class="nav-item @yield('active-teams')"> <a class="nav-link" href="{{route('teams',['locale'=>$locale])}}">{{__('general.team')}}</a> </li> --}}
              <li class="nav-item @yield('active-career')"> <a class="nav-link" href="{{route('career',['locale'=>$locale])}}">{{__('general.career')}}</a> </li>
              <li class="nav-item @yield('active-gallerys')"> <a class="nav-link" href="{{route('gallerys',['locale'=>$locale])}}">{{__('general.gallery')}}</a> </li>
              {{-- <li class="nav-item @yield('active-blogs')"> <a class="nav-link" href="{{route('blogs',['locale'=>$locale])}}">{{__('general.blog')}}</a> </li> --}}
              <li class="nav-item @yield('active-contact')"> <a class="nav-link" href="{{route('contact',['locale'=>$locale])}}">{{__('general.contact')}}</a> </li>
            </ul>
          </div>
        </nav>
        <!-- End Navigation --> 
      </div>
    </div>
  </div>
  <!-- End Header Sec --> 
</header>

<!-- End Header --> 
@yield('content')
<!-- Start Footer -->
<footer class="footer footer-bg"> 
  <!-- Start Footer top -->
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-xs-12">
          <div class="footer-detail"> <a href="{{route('home',['locale'=>$locale])}}" class="footer-logo"><img src="{{ asset('public/frontend/images/khn.png') }}" alt=""></a>
            <p>{{__('general.SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company')}}</p>
          </div>
        </div>
        <div class="col-sm-4 col-xs-12">
          <div class="imp-links">
            <h3>Important Links</h3>
            <ul class="clearfix imp_list">
              <li><a href="{{route('home',['locale'=>$locale])}}">{{__('general.home')}}</a></li>
              <li><a href="{{route('about',['locale'=>$locale])}}">{{__('general.about')}}</a></li>
              <li><a href="{{route('services',['locale'=>$locale])}}">{{__('general.service')}}</a></li>
              <li><a href="{{route('project',['locale'=>$locale])}}">{{__('general.project')}}</a></li>
              {{-- <li><a href="{{route('teams',['locale'=>$locale])}}">{{__('general.team')}}</a></li> --}}
              <li><a href="{{route('career',['locale'=>$locale])}}">{{__('general.career')}}</a></li>
              <li><a href="{{route('gallerys',['locale'=>$locale])}}">{{__('general.gallery')}}</a></li>
              {{-- <li><a href="{{route('blogs',['locale'=>$locale])}}">{{__('general.blog')}}</a></li> --}}
              <li><a href="{{route('contact',['locale'=>$locale])}}">{{__('general.contact')}}</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4 col-xs-12">
          <div class="contact-us">
            <h3>Get In Touch</h3>
            <a href="{{route('contact',['locale'=>$locale])}}"><p><i class="fa fa-map-marker" aria-hidden="true"></i>
              {{__('general.location')}}</p></a>
            <a href="tel:+855 15 855 558"><i class="fa fa-phone" aria-hidden="true"></i>+855 15 855 558</a> <a href="{{route('contact',['locale'=>$locale])}}"><i class="fa fa-envelope" aria-hidden="true"></i>lysenserey@senserey.com</a> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Footer top --> 
  
  <!-- Copy Rights -->
  <div class="copy-rights-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <p>{{__('general.© 2020 SENSEREY CONSTRUCTION CO., LTD.All rights reserved')}}</p>
        </div>
        <div class="col-sm-6 col-xs-12">
          <ul class="follow-us clearfix">
            <li><a href="https://www.facebook.com/SenseRey-Construction-Co-Ltd-1521235318114493/" target="_blank"><img src="{{ asset('public/frontend/images/fb.png') }}" alt="Facebook"></a></li>
            <!-- <li><a href="#" target="_blank"><img src="images/tweeter.png" alt="Tweeter"></a></li>
            <li><a href="#" target="_blank"><img src="images/pintrest.png" alt="Pintrest"></a></li>
            <li><a href="#" target="_blank"><img src="images/g-plush.png" alt="Google plus"></a></li>
            <li><a href="#" target="_blank"><img src="images/instagram.png" alt="Instagram"></a></li> -->
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- End Copy Rights --> 
</footer>
<!-- End Footer --> 

<!-- Scroll to top --> 
<a href="#" class="scroll-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="{{ asset('public/frontend/js/jquery.min.js') }}"></script> 
<!-- Bootsrap JS --> 
<script src="{{ asset('public/frontend/assets/bootstrap/js/bootstrap.min.js') }}"></script> 
<!-- Owl Carousal JS --> 
<script src="{{ asset('public/frontend/assets/owl-carousel/js/owl.carousel.min.js') }}"></script> 
<!-- Freewall JS --> 
<script type="text/javascript" src="{{ asset('public/frontend/assets/freewal/freewall.js') }}"></script>
<!-- Custom JS --> 
<script src="{{ asset('public/frontend/js/custom.js') }}"></script>
<script>

$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});

$(window).load(function(e) {

var wall = new Freewall("#freewall");
	wall.reset({
		selector: '.item',
		animate: true,
		cellW: 340,
		cellH: 245,
		gutterX: 30, // width spacing between blocks;
    gutterY: 30, // height spacing between blocks;
		onResize: function() {
			wall.refresh();
		}
	});

	$(".tabs li a").on("click", function(e){
		e.preventDefault();
		$(".tabs li").removeClass("active");
		$(this).parent('li').addClass('active');
		var filter = $(this).addClass('active').data('filter');
		if (filter) {
			wall.filter(filter);
		} else {
			wall.unFilter();
		}
	});
 
 setInterval(function(){ wall.fitWidth(); }, 300);
});
$(window).load(function(e) {
var wall = new Freewall("#freewall-1");
	wall.reset({
		selector: '.item',
		animate: true,
		cellW: 340,
		cellH: 245,
		gutterX: 30, // width spacing between blocks;
        gutterY: 30, // height spacing between blocks;
		onResize: function() {
			wall.refresh();
		}
	});

	$(".my .tabs li a").on("click", function(e){
		e.preventDefault();
		$(".my .tabs li").removeClass("active");
		$(this).parent('li').addClass('active');
		var filter = $(this).addClass('active').data('filter');
		if (filter) {
			wall.filter(filter);
		} else {
			wall.unFilter();
		}
	});
 
 setInterval(function(){ wall.fitWidth(); }, 300);
});
</script>
</body>
<!-- Mirrored from webdevproof.com/theme-forest-demo/onena/demo-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 May 2020 15:50:02 GMT -->
</html>