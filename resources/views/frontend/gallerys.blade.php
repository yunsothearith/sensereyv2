@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-gallerys', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')
@include('frontend/side.slide', ['slide' => $slide])

  <!-- Start feature sec -->
  <div class="feature-outer">
    <div class="container">
      <div class="head">
        <h3>{{__('general.our-people')}}</h3>
        <p>{{__('general.all-people')}}</p>
      </div>

      <div class="feature-list" id="freewall">
        <div class="row tab_container">
          @foreach( $gallery as $row)
          <div class="item {{$row->category->slug ?? ''}}">
            <div class="feature-box">
              <figure> <a data-fancybox="gallery" href="{{ asset ($row->image)}}"><img class="img-fluid" alt="Responsive image"  src="{{ asset ($row->image)}}"></a></figure>
            </div>
          </div>
          @endforeach        
        </div>
      </div>
    </div>
  </div>
  <!-- End feature sec --> 
@endsection
