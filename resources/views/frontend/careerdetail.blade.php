@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-career', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')
@include('frontend/side.slide', ['slide' => $slide])
	  <div class="col-sm-12">
		{{-- <h1 style="font-size: 24px;margin-top: 45px;;padding-left: 647px;">Career of SENSEREY Company</h1>	 --}}
	</div>
	  <div class="offeres-outer mt-5">
		<div class="container">
		<div class="row">
			<div class="col-sm-12">
				@foreach( $job as $row)
				<div style="margin-top:15px; margin-bottom: 15px;" class="imp-quote">
				<h1 style="font-size: 24px;margin-top: px;">{{$row->title}}</h1>	
				<p style="padding-left: 10px">{{$row->location}}</p>	
				<p style="padding-left: 10px">{{$row->description}}</p>		
				<p style="padding-left: 10px">{{$row->deadline}}</p>	
				</div>
				@endforeach
		  	</div>
		</div>
	  </div>
	</div>
@endsection