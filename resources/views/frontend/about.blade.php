@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-about', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

<!-- Start Content -->
<section class="content">

	@include('frontend/side.slide', ['slide' => $slide])

	<!-- Start About Detail -->
	<div class="about-detail-outer">
	  <div class="container">
		<div class="row">
			@foreach( $persident as $row)
		  	<div class="col-sm-6 col-xs-12">
				<figure><img src="{{ asset ($row->image)}}" style="margin-top: 10px;"></figure>
				<div class="persident" >
				<div>{{$row->role}}</div>
			</div>
			</div>
		 	 <div class="col-sm-6 col-xs-12">
				<h3>{{$row->title}}</h3>
				<p style="color: gray;">{{$row->description}}</p>
			</div>
		@endforeach
		</div>
	  </div>
	</div>
	<!-- End About Detail sec --> 
	<!-- Start About Detail -->
	<div class="about-detail-outer mt-5">
		<div class="container">
			<div class="row">
				@foreach( $history as $row)
				<div class="col-sm-6 col-xs-12">
					<div>
						<h3>{{$row->title}}</h3>
						<p style="color: gray;">{{$row->description}}</p>
					</div>
					<div class="btns">
						<a class="btn-download" style="border: 1px solid;" target="_blank"href="https://drive.google.com/file/d/14qM_gbAqH8QyaFHWTt2liMooRWa4fD-i/view?usp=sharing"><i class="fa fa-download"></i> Download Company Profile</a>
					</div>
					<br>
					{{-- <a href="contact-us.html" class="btn">Get a Free Quote</a>  --}}
				</div>
				<div class="col-sm-6 col-xs-12">
					<figure><img src="{{ asset ($row->image)}}" style=""></figure>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<!-- End About Detail sec --> 
	<!-- Start About Detail -->
	<div class="about-detail-outer mt-5">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						{{-- <ol class="carousel-indicators">
						  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol> --}}
						<div class="carousel-inner">
							@php($i = 1)
							@foreach( $ourpartner as $row)
							<div class="carousel-item  @if($i++ == 1) active @endif" >
							<img class=" w-100" src="{{ asset ($row->image)}}" alt="First slide">
						  </div>
						  @endforeach
						</div>
						<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						  <i class="fa fa-chevron-left" aria-hidden="true" ></i>
						</a>
						<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						  <span class="carousel-control-next-icon" aria-hidden="true"></span>
						  <i class="fa fa-chevron-right" aria-hidden="true"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					@foreach( $work as $row)
					<h3 style="">{{ $row->title }}</h3>
					<P>{{ $row->description }}</P>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<!-- End About Detail sec --> 

	<div class="head">
		<h3 style="color: #1b72b6;font-size: 30px;margin-top:30px;">{{__('general.certificate-regisration-documents')}}</h3>
	</div>
	<div class="service-outer blog-outer blox-box">
		<div class="container">
			<div class="service-list">
				<div class="row">
					@foreach( $certifiate as $row)
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="service-box">
								<figure><img src="{{ asset ($row->image)}}" alt=""></figure>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>

</section>
<!-- End Content sec --> 


@endsection