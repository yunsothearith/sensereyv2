@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-service', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')


<!-- Start Content -->
<section class="content">
	@include('frontend/side.slide', ['slide' => $slide])
	<!-- Start Banner -->
	<div class="banner-outer inner-banner"> <span class="banner-shadow"></span>
		<div class="banner-image service-banner-image">
			@foreach( $bannerservice as $row)
			<div class="content animated fadeIn">
				<h1 class="animated fadeIn">{{$row->title}}</h1>
				<p class="animated fadeIn">{{$row->description}}</p>
			  </div>
			  <img src="{{ asset ($row->image)}}" alt style="margin-top: -699px;width: 1905px; height: 588px;">
			  @endforeach
		</div>
	</div>
	<!-- End Banner -->

	<!-- Start Breadcrumbs -->
	{{-- <div class="breadcrumbs_outer">
	  <div class="container">
		<ul style="margin-top: -54px;"class="breadcrumbs">
		  <li><a href="index.html">Home</a> > </li>
		  <li>Services</li>
		</ul>
	  </div>
	</div> --}}
	<!-- End Breadcrumbs -->

 <!-- Start service sec -->
 <div class="service-outer">
    <div class="container">
      <div class="head">
        <h3>{{__('general.Our-Services')}}</h3>
        <p>{{__('general.Our-Services-senserey')}}</p>
      </div>
      <div class="service-list service-list2 service-slider owl-carousel">
		@foreach( $service as $row)
        <div class="item">
          <div class="service-box">
            <div class="service-detail">
			  <figure><a href="{{route('servicedetail',['locale'=>$locale, 'id' => $row->id])}}">
				<img src="{{ asset ($row->image)}}" alt="">
				</a>
				</figure>
              <a href="{{route('servicedetail',['locale'=>$locale, 'id' => $row->id])}}"><h4>{{ $row->name }}</h4></a>
            
			</div>
          </div>
		</div>
		@endforeach
        </div>
	  </div>
    </div>
  </div>
</section>
<!-- End Content sec --> 



	<!-- Start Blogs sec -->
	{{-- <div class="service-outer blox-box">
	  <div class="container">
		<div class="service-list">
		  <div class="row">
			@if(Count($service) >0) 
			@foreach( $service as $row)
			<div class="col-sm-4 col-xs-12">
			  <div class="service-box">
				<figure><img src="{{ asset ($row->image)}}" alt=""></figure>
				<div class="service-detail">
				  <h4>{{$row->title}}</h4>
				  <p>{{$row->description}}</p>
				  <a href="{{route('servicedetail',['locale'=>$locale])}}" class="btn">Know More</a> </div>
			  </div>
			</div>
			@endforeach
			@else
			<div class="col-12">
			  <div class="alert alert-warning" role="alert">
				គ្មាន​ទិន្នន័យ
			  </div>
			</div>
		  @endif
		  </div>
		</div>
	  </div>
	</div> --}}
	<!-- End Blogs sec -->

</section>
<!-- End Content sec -->  


@endsection