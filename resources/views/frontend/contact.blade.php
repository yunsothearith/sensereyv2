@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-contact', 'active')


@section ('appbottomjs')
<script src='https://www.google.com/recaptcha/api.js'></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  <script type="text/javascript">
    $(document).ready(function(){
      $("#submit-contact").submit(function(event){
        submit(event);
      })

      @if (Session::has('invalidData'))
        $("#form").show();
      @endif

    })
	@if(Session::has('msg'))
			toastr.success("{{ Session::get('msg') }}");
	@endif



    function submit(event){
      
	  name = $("#name").val();
      email = $("#email").val();
      phonenumber = $("#phonenumber").val();
      message = $("#message").val();
      recaptcha =$('#g-recaptcha-response').val();

      if(name != ''){
			
      if(validateEmail(email)){

        if(phone != ''){

        }else{
          toastr.error("Please enter your phone");
          event.preventDefault();
          $("#email").focus();
        }

      }else{
          toastr.error("Please give us correct email");
          event.preventDefault();
          $("#email").focus();
      }
              
      }else{
          toastr.error("Please enter your name");
          event.preventDefault();
          $("#fristname").focus();
      }
    }else{
          toastr.error("Please enter your name");
          event.preventDefault();
          $("#lastname").focus();
      }
    }
    
    function showApplicationForm(){
      form = $("#form");
      if(form.is(":visible")){
        form.hide();
      }else{
        form.show();
      }
    }

    
      function validatePhone(phone) {
          return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
      }

</script>
@endsection
@section ('home')
@endsection

@section ('content')

 <!-- Start Content -->
<section class="content">

	@include('frontend/side.slide', ['slide' => $slide])

	<!-- Start Contact sec -->
	<div class="contact-outer" >
	  <div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="contact-info clearfix">
				  <h3>{{__('general.contact-information')}}</h3>
				  <p><i class="fa fa-map-marker" aria-hidden="true"></i><span>{{__('general.location')}}</span></p><br>
				  <p> <i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+855 77 766 277" href="tel:+855 96 521 4502" href="tel:+855 23 432 255">+855 77 766 277 / 096 521 4502 / 023 432 255</a></p><br>
				  <p> <i class="fa fa-envelope" aria-hidden="true"></i><a href="">sothsamnang@senserey.com ,ngounchakriya@senserey.com </a></p>
				 
				  <div class="follow-us-outer">
					<h3>Follow Us</h3>
					<ul class="follow-us clearfix">
					  <li><a href="https://www.facebook.com/SenseRey-Construction-Co-Ltd-1521235318114493/" target="_blank"><img src="{{ asset('public/frontend/images/facebook.jpg') }}" alt="Facebook"></a></li>
					  <!-- <li><a href="#" target="_blank"><img src="images/tweeterB.png" alt="Tweeter"></a></li>
					  <li><a href="#" target="_blank"><img src="images/pintrestB.png" alt="pintrest"></a></li>
					  <li><a href="#" target="_blank"><img src="images/g-plushB.png" alt="Google plus"></a></li>
					  <li><a href="#" target="_blank"><img src="images/instagramB.png" alt="Instagram"></a></li> -->
					</ul>
				  </div>
				</div>
			  </div>
		  <div class="col-sm-12 col-xs-12">
			<div class="form-wrapper" >
			  <h3>{{__('general.mssege')}}</h3>
			  <form  id="submit-contact"  action="{{ route('submit-contact', $locale) }} " method="post">
				{{ csrf_field() }} 
                {{ method_field('PUT') }}
                <input style="display: none;" type="text" id="page" name="page" class="form-control" value="contact" required>
				<div class="row input-row">
				  <div class="col-sm-12">
					<input name="name" id="name" type="text" placeholder=" {{__('general.name')}}" required>
				  </div>
				</div>
				<div class="row input-row">
				  <div class="col-sm-6">
					<input name="email" id="email" type="email" placeholder="{{__('general.email')}}" required>
				  </div>
				  <div class="col-sm-6">
					<input name="phone" id="phone" type="number" placeholder="{{__('general.phone')}}" required>
				  </div>
				</div>
				<div class="row input-row">
				  <div class="col-sm-12">
					<textarea name="message" placeholder="{{__('general.message')}}"></textarea>
				  </div>
				</div>
				<div class="row">
				  <div class="col-sm-12">
					<input type="submit" name="submit" class="btn" value="Submit">
					<div class="msg"></div>
				  </div>
				</div>
			  </form>
			</div>
			@if (count($errors) > 0)
              @foreach ($errors->all() as $error)
                <div class="pd-buttom">
                  <div class="alert alert-danger" role="alert">
                    {{ $error }}
                  </div>
                </div>
              @endforeach
            @endif
            @if(Session::has('msg'))
              <div class="pd-buttom">
                <div class="alert alert-success" role="alert">
				sent successful
                </div>
              </div>
            @endif
		  </div>
		</div>
	  </div>
	</div>
	<!-- End Contact sec --> 
	<!-- Start Map sec -->
	<section class="clearfix mapSection">
		<div class="mapArea">
		  <div id="map">
		  </div>
		</div>
	  </section>
	<div class="map-outer">
		<iframe src = "https://maps.google.com/maps?q=11.5779686,104.9316695&hl=es;z=14&amp;output=embed"
		width="600" height="500" style="border:0" allowfullscreen></iframe>
	  {{-- <iframe 
	  src="https://maps.google.com/maps?q='11.5779686','104.9316695'&hl=es&z=14&amp;output=embed"
	  width="600" height="500" style="border:0" allowfullscreen>
	</iframe> --}}
	</div>
	<!-- End Map sec -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRQUxXDUoOYY5VGCKnOA6MuCIeuhzSn84" type="b3030b6b4fb376bf2cbb99e6-text/javascript"></script>
	<script src="{{ asset ('public/frontend/js/google-map.js')}}" type="b3030b6b4fb376bf2cbb99e6-text/javascript"></script>
	<script src="{{ asset ('public/frontend/js/custom.js')}}" type="b3030b6b4fb376bf2cbb99e6-text/javascript"></script>
	<script type="b3030b6b4fb376bf2cbb99e6-text/javascript">
	  //paste this code under head tag or in a seperate js file.
	  // Wait for window load
	  $(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	  });
	</script>
</section>
<!-- End Content sec -->  


@endsection