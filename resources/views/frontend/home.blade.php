@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-home', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

<!-- Start Content -->
<section class="content">

  @include('frontend/side.slide', ['slide' => $slide])

 <!-- Start service sec -->
 <div class="service-outer">
  <div class="container">
    <div class="head">
      <h3>{{__('general.Our-Services')}}</h3>
      <p>{{__('general.Our-Services-senserey')}}</p>
    </div>
    <div class="service-list service-list2 service-slider owl-carousel">
        @foreach( $service as $row)
          <div class="item">
            <div class="service-box">
              <div class="service-detail">
                <figure><a href="{{route('servicedetail',['locale'=>$locale, 'id' => $row->id])}}"><img src="{{ asset ($row->image)}}" alt=""></a></figure>
                <a href="{{route('servicedetail',['locale'=>$locale, 'id' => $row->id])}}"><h4>{{ $row->name }}</h4></a>
                <p>{{ $row->description }}</p>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
<!-- End service sec --> 
  
  <!-- Start offer sec -->
  <div class="offer-outer">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          @foreach( $trained as $row)
          <h3>{{$row->title}}</h3>
          <p>{{$row->description}}</p> 
          <a href="tel:+855 16637613"class="btn">Call Us Now</a>
          @endforeach
        </div>
        <div class="col-sm-6 col-xs-12">
          <ul class="offer-list">
            @foreach( $icontrained as $row)
            <li>
              <figure><img src="{{ asset ($row->image)}}" alt=""></figure>
              <span>{{$row->title}}<br>
              </span>
             </li>
             @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- End Offer sec --> 

   <!-- Start feature sec -->
   <div class="feature-outer">
    <div class="container">
      <div class="head">
        <h3>{{__('general.project-progress')}}</h3>
        <p>{{__('general.Our-Services-senserey')}}</p>
      </div>
      <div class="my">
        <ul class="tabs">
          <li class="active"><a href="#">{{__('general.all-group')}}</a></li>
          @foreach($categories as $row)
            <li class=""><a href="#" data-filter=".{{$row->slug}}-1">{{$row->title}}</a></li>
          @endforeach
        </ul>
      </div>
      <div class="feature-list" id="freewall-1">
        <div class="row tab_container">
          @foreach($catelogProgress as $row)
				  <div class="item {{$row->category->slug ?? ''}}-1">
					<div class="feature-box">
					  <figure><img src="{{ asset($row->image) }}" alt=""></figure>
					  <div class="thumb-overlay">
						<div class="thumb-overlay-inner"> <a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}">
						<h5>{{$row->name}}</h5>
						  <p>Location :KOI Thé WB Arena, 2, NR2, Phnom Penh</p>
						  </a> </div>
					  </div>
					</div>
				  </div>
				    @endforeach
        </div>
      </div>
    </div>
  </div>
  <!-- End feature sec --> 
  
  <!-- Start feature sec -->

  <div class="feature-outer">
    <div class="container">
      <div class="head">
        <h3>{{__('general.complet-project')}}</h3>
        <p>{{__('general.company-is-involved-in-every-aspect-of-a-construction-and-experienced')}}</p>
      </div>
      <ul class="tabs">
        <li class="active"><a href="#">{{__('general.all-group')}}</a></li>
        @foreach($categories as $row)
          <li class=""><a href="#" data-filter=".{{$row->slug}}">{{$row->title}}</a></li>
        @endforeach
      </ul>
      <div class="feature-list" id="freewall">
        <div class="row tab_container">
          @foreach($catelog as $row)
          {{-- @php(dd($row->category->slug)) --}}
            <div class="item {{$row->category->slug ?? ''}}">
              <div class="feature-box">
                <figure><a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}"><img src="{{ asset($row->image) }}" alt=""></a></figure>
                <div class="thumb-overlay">
                <div class="thumb-overlay-inner"> <a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}">
                <h5>{{$row->name}}</h5>
                  <p>Location :KOI Thé WB Arena, 2, NR2, Phnom Penh</p>
                  </a> </div>
                </div>
              </div>
            </div>
				    @endforeach
        </div>
      </div>
    </div>
  </div>

  {{-- <div class="feature-outer">
    <div class="container">
      <div class="head">
        <h3>Completd Projects</h3>
        <p>company is involved in every aspect of a construction and experienced.</p>
      </div>
      <ul class="tabs">
        <li class="active"><a href="#">All Groups</a></li>
        <li><a href="#"  data-filter=".commercial">Design</a></li>
        <li><a href="#"  data-filter=".education">Construction</a></li>
        <li><a href="#"  data-filter=".hospital">Hospital</a></li>
        <li><a href="#"  data-filter=".office">Office</a></li>
        <li><a href="#"  data-filter=".residential">Residential</a></li>
      </ul>
      <div class="feature-list" id="freewall">
        <div class="row tab_container">
          @foreach($catelog as $row)
				  <div class="item residential">
					<div class="feature-box">
					  <figure><img src="{{ asset($row->image) }}" alt=""></figure>
					  <div class="thumb-overlay">
						<div class="thumb-overlay-inner"> <a href="{{route('projectdetail',['locale'=>$locale, 'id' => $row->id])}}">
						<h5>{{$row->name}}</h5>
						  <p>Location :KOI Thé WB Arena, 2, NR2, Phnom Penh</p>
						  </a> </div>
					  </div>
					</div>
				  </div>
				    @endforeach
        </div>
      </div>
    </div>
  </div> --}}
  
  <!-- Start Our Partners -->
  <div class="partners-outer">
    <div class="container">
      <div class="head">
        <h3>{{__('general.partner')}}</h3>
        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rutrum scelerisque porta. Nam quis nisl dui. Mauris rhoncus metus quis velit elementum elementum.</p> -->
      </div>
      {{-- <div class="partners-list">
        <div class="row">
          @foreach( $partner as $row)
          <div class="col-sm-4 col-xs-12">
            <div class="partners-box"> 
              <figure><img src="{{ asset ($row->image)}}" class="img-center " alt=""></figure>
            </div>
          </div>
          @endforeach
        </div>
      </div> --}}

      <div class="service-list service-list2 service-slider owl-carousel">
        @foreach( $partner as $row)
          <div class="item">
            <div class="partners-box">
              <div class="service-detail">
                <figure><a href=""><img src="{{ asset ($row->image)}}" alt=""></a></figure>
              </div>
            </div>
          </div>
        @endforeach
      </div>

    </div>
  </div>
<!-- Start Blogs sec -->
{{-- <div class="head">
        <h3>Blog</h3>
        <p>Find our ast post news here</p>
      </div>
<div class="service-outer blog-outer blox-box">
  <div class="container">
  <div class="service-list">
    <div class="row">
			@if(Count($blog) >0)
			@foreach( $blog as $row)
			<div class="col-md-4 col-sm-6 col-xs-12">
			  <div class="service-box">
				<figure><a href="{{route('blogdetail',['locale'=>$locale ,'id' => $row->id])}}"><img src="{{ asset ($row->image)}}" alt=""></a></figure>
				<div class="service-detail">
				  <a href="{{route('blogdetail',['locale'=>$locale ,'id' => $row->id])}}" ><h4>{{$row->title}}</h4></a>
            <a href="{{route('blogdetail',['locale'=>$locale ,'id' => $row->id])}}"> <p style="color: black">{{$row->description}}</p></a>
				  </div>
			  </div>
			</div>
			@endforeach
			@else
			<div class="col-12">
				<div class="alert alert-warning" role="alert">
					គ្មាន​ទិន្នន័យ
				</div>
			</div>
		@endif
		  </div>
  </div>
  </div>
</div> --}}
<!-- End Blogs sec --> 
  <!-- End Our Partners --> 
  <!-- Start Virtually  Build -->
  {{-- <div class="build-outer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-xs-12">
          <h3>Virtually  Build Your House</h3>
          <h4>Perfection is Always<br/>
            Under Construction</h4>
          <a href="{{route('about',['locale'=>$locale])}}" class="btn">Know More About Us</a> </div>
      </div>
    </div>
  </div> --}}
  <!-- End Virtually  Build --> 
  <!-- Start Blogs sec -->
  <!-- <div class="service-outer blox-box">
    <div class="container">
      <div class="head">
        <h3>Latest From the Blog</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rutrum scelerisque porta. Nam quis nisl dui. Mauris rhoncus metus quis velit elementum elementum.</p>
      </div>
      <div class="service-list">
        <div class="row">
          <div class="col-sm-4 col-xs-12">
            <div class="service-box">
              <figure><img src="images/service_img4.png" alt=""></figure>
              <div class="service-detail">
                <h4>Morbi eleifend dui vel <br/>
                  magna tincidunt tinci...</h4>
                <p>Pellentesque vitae dui sit amet enim consequat bibendum. Donec feugiat ex arcu, sit amet rhoncus neque viverra.</p>
                <a href="blog-detail.html" class="btn">Read More</a> </div>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="service-box">
              <figure><img src="images/service_img5.png" alt=""></figure>
              <div class="service-detail">
                <h4>Morbi eleifend dui vel <br/>
                  magna tincidunt tinci...</h4>
                <p>Pellentesque vitae dui sit amet enim consequat bibendum. Donec feugiat ex arcu, sit amet rhoncus neque viverra.</p>
                <a href="blog-detail.html" class="btn">Read More</a> </div>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="service-box">
              <figure><img src="images/service_img6.png" alt=""></figure>
              <div class="service-detail">
                <h4>Morbi eleifend dui vel <br/>
                  magna tincidunt tinci...</h4>
                <p>Pellentesque vitae dui sit amet enim consequat bibendum. Donec feugiat ex arcu, sit amet rhoncus neque viverra.</p>
                <a href="blog-detail.html" class="btn">Read More</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->
  <!-- End Blogs sec --> 
</section>
<!-- End Content sec --> 


@endsection