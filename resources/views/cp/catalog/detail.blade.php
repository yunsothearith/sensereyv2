@extends('frontend.layouts.master')
@section('active-catalog', 'active-menu')
@section ('content')

		<div class="page-banner2-area">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="">
							<h2><strong>{{__('general.catelogs')}}</strong></h2>
						</div>
						<div class="breadcrumb-content">
							<ul>
								<li><a href="{{route('home',$locale)}}">{{__('general.home')}}</a></li>
								<li class=""><a href="{{route('catalog-detail', ['locale'=>$locale, 'id'=>$data->id ?? ''])}}">{{$data->name ?? ''}}</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		

		<div class="protfolio-item mb-30">
            <div class="container-fluid">
                <div class="row protfolio-active" style="position: relative; height: 1168.75px;">  
					
					@if($galleries)
						@foreach($galleries as $row)
							<div class="col-md-6 col-lg-4 col-xl-3 grid-item company">
								<div class="single-protfolio">
									<div class="protfolio-img img-full">
										<img src="{{ asset($row->image ?? '') }}" alt="">
										<!-- <a class="venobox vbox-item" data-gall="myGallery" href="{{ asset($row->image ?? '') }}">
											<i class="fa fa-search"></i>
										</a> -->
										{{-- <div class="link">
											<a target="_blank" href="{{url($row->link)}}" ><i class="fa fa-link"></i></a>
										</div> --}}
									</div>
									<div class="protfolio-info">
									{{-- <h3><a target="_blank" href="{{url($row->link)}}" >{{$row->title}}</a></h3> --}}
									{{-- <p><a target="_blank" href="{{url($row->link)}}" >{{__('general.download')}}</a></p> --}}
									</div>
								</div>
							</div>
						@endforeach
					@endif
                </div>
            </div>
        </div>	
	@endsection