@extends('frontend.layouts.master')
@section ('content')
@section('active-catalog', 'active-menu')
       

    <!--Header Area End-->
<div class="page-banner2-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <h2><strong>{{__('general.catelogs')}}</strong></h2>
                    </div>
                    <div class="breadcrumb-content">
                        <ul>
                            <li><a href="index.html">{{__('general.home')}}</a></li>
                            <li class="active"><a href="{{route('catalog', ['locale'=>$locale])}}">{{__('general.catelogs')}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--Banner Area Start-->
<div class="banner-area mb-60">
        <div class="container-fluid">
            <div class="row" style="margin-bottom:20px;">
              
                @foreach($defaultData['mainProduct'] as $row)
                <div class="col-md-4 mb-3">
                    <!--Anadi Single banner Start-->
                    <div class="single-product">
                        <div class="product-img">
                        <a href="{{route('catalog', ['locale'=>$locale])}}?type={{GetMainProductType($row->id)}}">
                                <img src="{{asset($row->image)}}" alt="">
                            </a>
                            
                            <div class="product-action">
                                <div class="quickviewbtn">
                                    <a href="{{route('catalog', ['locale'=>$locale])}}?type={{GetMainProductType($row->id)}}" >{{$row->name}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Anadi Single banner End-->
                </div>
                @endforeach
               
            </div>
            <div class="product-pagination">
              
            </div>

        </div>
    </div>
    <!--Product Area End-->

@endsection
		