@extends($route.'.tab')
@section ('section-title', 'Edit File')
@section ('about-active-galleries', 'active')
@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	<script>
		var btnCust = ''; 
		$("#image").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
			defaultPreviewContent: '<img src="{{ asset($data->image) }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 800x700 with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif","jpng"]
		});
	</script>
@endsection

@section ('tap-content')
	<div class="container-fluid">
		@include('cp.layouts.error')
		<form id="form" action="{{ route('cp.catelogs.update-gallery', [ 'id' => $id, 'gallery_id'=>$data->id]) }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_description">Description (KH) </label>
				<div class="col-sm-10">
					<input 	id="kh_description"
							name="kh_description"
						   	value = "{{$data->kh_description}}"
						   	type="text"
						   	placeholder = "Enter description kh."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_description">Description (EN) </label>
				<div class="col-sm-10">
					<input 	id="en_description"
							name="en_description"
						   	value = "{{$data->en_description}}"
						   	type="text"
						   	placeholder = "Enter description en."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_description">Description (CN) </label>
				<div class="col-sm-10">
					<input 	id="cn_description"
							name="cn_description"
						   	value = "{{$data->cn_description}}"
						   	type="text"
						   	placeholder = "Enter description cn."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			{{-- <div class="form-group row">
				<label class="col-sm-2 form-control-label" for="link">Link</label>
				<div class="col-sm-10">
					<input 	id="link"
							name="link"
						   	value = "{{$data->link}}"
						   	type="text"
						   	placeholder = "Enter link"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div> --}}
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Image</label>
				<div class="col-sm-10">
					<div class="kv-avatar center-block">
				        <input id="image" name="image" type="file" class="file-loading">
				    </div>
				</div>
			</div>
			
			<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_content">Published</label>
					<div class="col-sm-10">
						<div class="checkbox-toggle">
							<input id="status-status" name="status" type="checkbox"  @if($data->is_published ==1 ) checked @endif >
							<label onclick="booleanForm('status')" for="status-status"></label>
						</div>
					</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <i class="fa fa-cog"></i> Update</button>
				</div>
			</div>
		</form>
	</div>

@endsection