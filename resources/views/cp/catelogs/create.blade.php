@extends($route.'.main')
@section ('section-title', 'Create Project')
@section ('about-active-overview', 'active')
@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	<script>
		
		var btnCust = ''; 
		$("#image").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="http://via.placeholder.com/358x252" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 358x252 with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif","jpng"]
		});
	</script>
@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('cp.layouts.error')
		@php ($kh_name = "")
		@php ($en_name = "")
		@php ($cn_name = "")
		@php ($kh_location = "")
		@php ($en_location = "")
		@php ($cn_location = "")
		@php ($en_service = "")
		@php ($kh_service = "")
		@php ($cn_service = "")
		@php ($type = "")

      

       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($kh_name = $invalidData['kh_name'])
			@php ($en_name = $invalidData['en_name'])
			@php ($cn_name = $invalidData['cn_name'])
			@php ($kh_location = $invalidData['kh_location'])
			@php ($en_location = $invalidData['en_location'])
			@php ($cn_location = $invalidData['cn_location'])
			@php ($en_service = $invalidData['en_service'])
			@php ($kh_service = $invalidData['kh_service'])
			@php ($cn_service = $invalidData['cn_service'])
			@php ($type = $invalidData['type'])
           
       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			{{-- <input type="hidden" name="id" value="{{ $product_id }}"> --}}

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="category">Category</label>
				<div class="col-sm-10">
					 <select class="form-control" id="category" name="category">
					 <option value="0">Select Category</option>
					   @foreach ($categories as $row)
						  <option value="{{ $row->id }}">{{ $row->en_title }}</option>
						@endforeach

					  </select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_name">Name (KH) </label>
				<div class="col-sm-10">
					<input 	id="kh_name"
							name="kh_name"
						   	value = "{{$kh_name}}"
						   	type="text"
						   	placeholder = "Enter name kh."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>


			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_name">Name (EN) </label>
				<div class="col-sm-10">
					<input 	id="en_name"
							name="en_name"
						   	value = "{{$en_name}}"
						   	type="text"
						   	placeholder = "Enter name en."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_name">Name (CN) </label>
				<div class="col-sm-10">
					<input 	id="cn_name"
							name="cn_name"
						   	value = "{{$cn_name}}"
						   	type="text"
						   	placeholder = "Enter name cn."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_location">Location (kH) </label>
				<div class="col-sm-10">
					<input 	id="kh_location"
							name="kh_location"
						   	value = "{{$kh_location}}"
						   	type="text"
						   	placeholder = "Enter location kh."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_location">Location (EN) </label>
				<div class="col-sm-10">
					<input 	id="en_location"
							name="en_location"
						   	value = "{{$en_location}}"
						   	type="text"
						   	placeholder = "Enter location en."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_location">Location (CN) </label>
				<div class="col-sm-10">
					<input 	id="cn_location"
							name="cn_location"
						   	value = "{{$cn_location}}"
						   	type="text"
						   	placeholder = "Enter location cn."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_service">Service(EN) </label>
				<div class="col-sm-10">
					<input 	id="en_service"
							name="en_service"
						   	value = "{{$en_service}}"
						   	type="text"
						   	placeholder = "Enter service en."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_service">Service(KH) </label>
				<div class="col-sm-10">
					<input 	id="kh_service"
							name="kh_service"
						   	value = "{{$kh_service}}"
						   	type="text"
						   	placeholder = "Enter service kh."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_service">Service(CN)</label>
				<div class="col-sm-10">
					<input 	id="cn_service"
							name="cn_service"
						   	value = "{{$cn_service}}"
						   	type="text"
						   	placeholder = "Enter service cn."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="type">Type</label>
				<div class="col-sm-10">
					<input 	id="type"
							name="type"
						   	value = "{{$type}}"
						   	type="text"
						   	placeholder = "Enter type."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="image">Image</label>
				<div class="col-sm-10">
					<div class="kv-avatar center-block">
				        <input id="image" name="image" type="file" class="file-loading">
				    </div>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Published</label>
				<div class="col-sm-10">
					<div class="checkbox-toggle">
						<input id="status-status" type="checkbox"  >
						<label onclick="booleanForm('status')" for="status-status"></label>
					</div>
					<input type="hidden" name="status" id="status" value="">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="is_progress">Progress</label>
				<div class="col-sm-10">
					<div class="checkbox-toggle">
						<input id="status-progress" type="checkbox"  >
						<label onclick="booleanForm('is_progress')" for="status-progress"></label>
					</div>
					<input type="hidden" name="is_progress" id="is_progress" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <i class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection