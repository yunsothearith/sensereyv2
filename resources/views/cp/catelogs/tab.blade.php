@extends('cp.catelogs.main')
@section ('tab-active-catelog', 'active')

@section ('section-content')
	
	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				<li><a href="{{ route('cp.catelogs.edit', [ 'id'=>$id]) }}" class="@yield ('about-active-overview')">Overview</a></li>
			
				<li><a href="{{ route('cp.catelogs.list-gallery', [ 'id'=>$id]) }}" class="@yield ('about-active-galleries')">File</a></li>
               
			</ul>
		</nav>

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('tap-content')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection