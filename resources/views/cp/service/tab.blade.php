@extends('cp.service.main')
@section ('tab-active-catelog', 'active')

@section ('section-content')

	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				<li><a href="{{ route('cp.service.edit', [ 'id'=>$id]) }}"         class="@yield ('about-active-overview')">Overview</a></li>
				<li><a href="{{ route('cp.service.list-gallery', [ 'id'=>$id]) }}" class="@yield ('about-active-gallery')">File</a></li>
				<li><a href="{{ route('cp.service.list-service', [ 'id'=>$id]) }}" class="@yield ('about-active-service-list')">Service Lists</a></li>
			</ul>
		</nav>

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('tap-content')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection