@extends('cp.product.main')
@section ('tab-active-product', 'active')

@section ('section-content')
	
	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				<li><a href="{{ route('cp.product.edit', $id) }}" class="@yield ('about-active-overview')">Overview</a></li>
				
				<li><a href="{{ route('cp.product.content', $id) }}" class="@yield ('about-active-content')">Content</a></li>
				<li><a href="{{ route('cp.product.logo', $id) }}" class="@yield ('about-active-logo')">Logo</a></li>
				@if($id == 2)
				<li><a href="{{ route('cp.product.banner', $id) }}" class="@yield ('about-active-banner')">Banner</a></li>
				@endif
				@if($id == 3 || $id == 2)
				<li><a href="{{ route('cp.product.list-type', $id) }}" class="@yield ('about-active-types')">Types</a></li>
				@endif
                <li><a href="{{ route('cp.product.list-item', $id) }}" class="@yield ('about-active-items')">Products</a></li>
			</ul>
		</nav>

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('tap-content')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection