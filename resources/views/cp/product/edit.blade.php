@extends($route.'.tab')
@section ('section-title', $data->en_name ?? '')
@section ('about-active-overview', 'active')

@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	<script>
		
		$(document).ready(function() {
			$("#btn-search").click(function(){
				search();
			})
		});
		function search(){
			key 	= $('#key').val();
			d_from 		= $('#from').val();
			d_till 		= $('#till').val();
			limit 		= $('#limit').val();

			url="?limit="+limit;
			if(key!=""){
				url+='&key='+key;
			}
			if(isDate(d_from)){
				if(isDate(d_till)){
					url+='&from='+d_from+'&till='+d_till;
				}
			}
			$(location).attr('href', '{{ route($route.'.edit',$id) }}'+url);
		}

		var btnCust = ''; 
		$("#image").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="{{ asset($data->image) }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 370x230 with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif","jpng"]
		});
	</script>
	
@endsection

@section ('tap-content')

	@include('cp.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">
		
			<div class="form-group row">
				<div class="col-md-6">
					<label class="col-sm-3 form-control-label" for="kh_name">Name (KH) </label>
					<div class="col-sm-9">
						<input 	id="kh_name"
								name="kh_name"
								value = "{{$data->kh_name}}"
								type="text"
								placeholder = "Enter Name in Khmer."
								class="form-control"
								data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>
				
				<div class="col-md-6">
					<label class="col-sm-3 form-control-label" for="en_name">Name (EN) </label>
					<div class="col-sm-9">
						<input 	id="en_name"
								name="en_name"
								value = "{{$data->en_name}}"
								type="text"
								placeholder = "Enter Name in English."
								class="form-control"
								data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>

			</div>

			<div class="form-group row">
					<div class="col-md-6">
						<label class="col-sm-3 form-control-label" for="kh_description">Description (KH) </label>
						<div class="col-sm-9">
							<input 	id="kh_description"
									name="kh_description"
									value = "{{$data->kh_description}}"
									type="text"
									placeholder = "Enter Description in Khmer."
									class="form-control"
									data-validation="[L>=1, L<=1000]"
									data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
									
						</div>
					</div>

					
					<div class="col-md-6">
						<label class="col-sm-3 form-control-label" for="en_description">Description (EN) </label>
						<div class="col-sm-9">
							<input 	id="en_description"
									name="en_description"
									value = "{{$data->en_description}}"
									type="text"
									placeholder = "Enter Description in English."
									class="form-control"
									data-validation="[L>=1, L<=1000]"
									data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
									

						</div>
					</div>
	
			</div>

			<div class="form-group row">
					<div class="col-md-6">
							<label class="col-sm-3 form-control-label" for="email">Image</label>
							<div class="col-sm-9">
								<div class="kv-avatar center-block">
									<input id="image" name="image" type="file" class="file-loading">
								</div>
							</div>
					</div>
					
					<div class="col-md-6">
							<label class="col-sm-3 form-control-label" for="kh_content">Published</label>
							<div class="col-sm-3">
								<div class="checkbox-toggle">
									<input id="status-status" name="status" type="checkbox"  @if($data->is_published ==1 ) checked @endif >
									<label onclick="booleanForm('status')" for="status-status"></label>
								</div>
							</div>

							<div class="col-sm-6">
								<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
							</div>
					</div>
			</div>
	</form>
@endsection
