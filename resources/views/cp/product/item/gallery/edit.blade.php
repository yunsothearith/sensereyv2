@extends($route.'.item.tabItem')
@section ('section-title', 'Edit Banner')
@section ('about-active-items', 'active')
@section ('tab-active-gallery', 'active')
@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
		.panel-body{
			height: 100px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	<script>
		var btnCust = ''; 
		$("#image").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
			defaultPreviewContent: '<img src="{{ asset($data->image) }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 370x230 with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif","jpng"]
		});
	</script>
@endsection

@section ('tab-item-content')
	<div class="container-fluid">
		@include('cp.layouts.error')
		<form id="form" action="{{ route('cp.product.update-gallery', ['id' => $id, 'item_id'=>$item_id, 'gallery_id'=>$data->id]) }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			@if($id == 2)
				<div class="form-group row">
					<label for="type_id" class="col-sm-2 form-control-label">Type</label>
					<div class="col-sm-10">
						<select id="type_id" name="type_id" class="form-control">
							<option value="">Select Type</option>
							@foreach($types as $row)
								<option value="{{$row->id}}" @if($row->id == $data->type_id) selected @endif >{{$row->en_name}}</option>
							@endforeach
						</select>
					</div>
				</div>
			@endif
			@if ($id == 2 || $id == 3)
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="code">Code </label>
				<div class="col-sm-10">
					<input 	id="code"
							name="code"
								value = "{{$data->code}}"
								type="text"
								placeholder = "Enter Product Code."
								class="form-control"
								data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			@endif
			@if ($id == 2)
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_name">Name (KH) </label>
					<div class="col-sm-10">
						<input 	id="kh_name"
								name="kh_name"
									value = "{{$data->kh_name}}"
									type="text"
									placeholder = "Enter Name in Khmer."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="en_name">Name (EN) </label>
					<div class="col-sm-10">
						<input 	id="en_name"
								name="en_name"
									value = "{{$data->en_name}}"
									type="text"
									placeholder = "Enter Name in English."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_volume">Volume (KH) </label>
					<div class="col-sm-10">
						<input 	id="kh_volume"
								name="kh_volume"
									value = "{{$data->kh_volume}}"
									type="text"
									placeholder = "Enter Volume in Khmer."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="en_volume">Volume (EN) </label>
					<div class="col-sm-10">
						<input 	id="en_volume"
								name="en_volume"
									value = "{{$data->en_volume}}"
									type="text"
									placeholder = "Enter Volume in English."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_height">Height (KH) </label>
					<div class="col-sm-10">
						<input 	id="kh_height"
								name="kh_height"
									value = "{{$data->kh_height}}"
									type="text"
									placeholder = "Enter Height in Khmer."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="en_height">Height (EN) </label>
					<div class="col-sm-10">
						<input 	id="en_height"
								name="en_height"
									value = "{{$data->en_height}}"
									type="text"
									placeholder = "Enter Height in English."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_diameter">Diameter (KH) </label>
					<div class="col-sm-10">
						<input 	id="kh_diameter"
								name="kh_diameter"
									value = "{{$data->kh_diameter}}"
									type="text"
									placeholder = "Enter Diameter in Khmer."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="en_diameter">Diameter (EN) </label>
					<div class="col-sm-10">
						<input 	id="en_diameter"
								name="en_diameter"
									value = "{{$data->en_diameter}}"
									type="text"
									placeholder = "Enter Diameter in English."
									class="form-control"
									data-validation="[L>=1, L<=200]"
								data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
								
					</div>
				</div>

				<div class="form-group row">
						<label class="col-sm-2 form-control-label" for="kh_item_number">Item Number (KH) </label>
						<div class="col-sm-10">
							<input 	id="kh_item_number"
									name="kh_item_number"
										value = "{{$data->kh_item_number}}"
										type="text"
										placeholder = "Enter Item Number in Khmer."
										class="form-control"
										data-validation="[L>=1, L<=200]"
									data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
									
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label" for="en_item_number">Item Number (EN) </label>
						<div class="col-sm-10">
							<input 	id="en_item_number"
									name="en_item_number"
										value = "{{$data->en_item_number}}"
										type="text"
										placeholder = "Enter Item Number in English."
										class="form-control"
										data-validation="[L>=1, L<=200]"
									data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
									
						</div>
					</div>

				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_content">Kh Content</label>
					<div class="col-sm-10">
						<div class="summernote-theme-1">
							<textarea  id="kh_content" name="kh_content" class="form-control summernote">{{$data->kh_content}} </textarea>
						</div>	
					</div>
				</div>
	
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="en_content">En Content</label>
					<div class="col-sm-10">
						<div class="summernote-theme-1">
							<textarea  id="en_content" name="en_content" class="form-control summernote">{{$data->en_content}} </textarea>
						</div>	
					</div>
				</div>
			@endif

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Image</label>
				<div class="col-sm-10">
					<div class="kv-avatar center-block">
				        <input id="image" name="image" type="file" class="file-loading">
				    </div>
				</div>
			</div>
			
			<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_content">Published</label>
					<div class="col-sm-10">
						<div class="checkbox-toggle">
							<input id="status-status" name="status" type="checkbox"  @if($data->is_published ==1 ) checked @endif >
							<label onclick="booleanForm('status')" for="status-status"></label>
						</div>
					</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				</div>
			</div>
		</form>
	</div>

@endsection