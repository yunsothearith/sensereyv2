@extends($route.'.item.tabItem')
@section ('about-active-items', 'active')
@section ('tab-active-color', 'active')

@section ('tab-item-content')
    <section class="box-typical files-manager">
        <nav class="files-manager-side" style="height: auto;">
            <ul class="files-manager-side-list">
                <li><a href="{{ route($route.'.edit-color', ['id'=>$id, 'item_id'=> $item_id, 'color_id'=>$item_color_id]) }}" class="@yield ('tab-color-active-overview')">Color Detail</a></li>
                <li><a href="{{ route($route.'.list-color-size', ['id'=>$id, 'item_id'=> $item_id, 'color_id'=>$item_color_id]) }}" class="@yield ('tab-color-active-size')">Size</a></li>
            </ul>
        </nav>

        <div class="files-manager-panel">
            <div class="files-manager-panel-in">
                <div class="container-fluid">
                    @yield ('tab-color-content')
                </div>
            </div><!--.files-manager-panel-in-->
        </div><!--.files-manager-panels-->
    </section>
				
@endsection