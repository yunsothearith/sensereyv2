@extends($route.'.item.color.tab')
@section ('section-title', 'Edit Color Size')
@section ('about-active-items', 'active')
@section ('tab-active-color', 'active')
@section ('tab-color-active-size', 'active')
@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	<script>
		var btnCust = ''; 
		$("#image").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
			defaultPreviewContent: '<img src="{{ asset($data->image) }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif","jpng"]
		});
	</script>
@endsection

@section ('tab-color-content')
	<div class="container-fluid">
		@include('cp.layouts.error')
		<form id="form" action="{{ route('cp.product.update-color-size', ['id' => $id, 'item_id'=>$item_id, 'item_color_id'=>$item_color_id, 'item_color_size_id'=>$item_color_size_id]) }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_name">Name (KH) </label>
				<div class="col-sm-10">
					<input 	id="kh_name"
							name="kh_name"
						   	value = "{{$data->kh_name}}"
						   	type="text"
						   	placeholder = "Enter Name in Khmer."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_name">Name (EN) </label>
				<div class="col-sm-10">
					<input 	id="en_name"
							name="en_name"
						   	value = "{{$data->en_name}}"
						   	type="text"
						   	placeholder = "Enter Name in English."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="product_code">Product Code </label>
				<div class="col-sm-10">
					<input 	id="product_code"
							name="product_code"
						   	value = "{{$data->product_code}}"
						   	type="text"
						   	placeholder = "Enter Product Code."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="size">Size </label>
				<div class="col-sm-10">
					<input 	id="size"
							name="size"
						   	value = "{{$data->size}}"
						   	type="text"
						   	placeholder = "Enter Size."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="series">Series </label>
				<div class="col-sm-10">
					<input 	id="series"
							name="series"
						   	value = "{{$data->series}}"
						   	type="text"
						   	placeholder = "Enter Series."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="color">Color </label>
				<div class="col-sm-10">
					<input 	id="color"
							name="color"
						   	value = "{{$data->color}}"
						   	type="text"
						   	placeholder = "Enter color."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="technology">Technology </label>
				<div class="col-sm-10">
					<input 	id="technology"
							name="technology"
						   	value = "{{$data->technology}}"
						   	type="text"
						   	placeholder = "Enter technology."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="structure">SureFace Look </label>
				<div class="col-sm-10">
					<input 	id="structure"
							name="structure"
						   	value = "{{$data->structure}}"
						   	type="text"
						   	placeholder = "Enter structure."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />	
				</div>
			</div>


			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Image</label>
				<div class="col-sm-10">
					<div class="kv-avatar center-block">
				        <input id="image" name="image" type="file" class="file-loading">
				    </div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				</div>
			</div>
		</form>
	</div>

@endsection