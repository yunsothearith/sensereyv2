@extends('cp.product.tab')
@section ('section-css')
	@yield ('tab-css')
@endsection

@section ('section-js')
	@yield ('tab-js')
@endsection

@section ('tap-content')
		
			
	<section class="tabs-section">
		<div class="tabs-section-nav tabs-section-nav-icons">
			<div class="tbl">
				<ul class="nav" role="tablist">
					<li class="nav-item">
						
						<a class="nav-link @yield ('tab-active-edit')" onclick="window.location.href='{{ route('cp.product.edit-item', ['id' => $id, 'item_id' => $item_id]) }}'" href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="glyphicon glyphicon-stats"></i> Overview 
							</span>
						</a>
					</li>
					@if($id == 1)
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-color')" onclick="window.location.href='{{ route('cp.product.list-color', ['id' => $id, 'item_id' => $item_id]) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-adjust"></i> Color
							</span>
						</a>
					</li>
					@endif

					@if($id == 1)
					{{-- <li class="nav-item">
						<a class="nav-link @yield ('tab-active-detail')" onclick="window.location.href='{{ route('cp.product.list-detail', ['id' => $id, 'item_id' => $item_id]) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-info"></i> Detail
							</span>
						</a>
					</li> --}}
					@endif
					
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-gallery')" onclick="window.location.href='{{ route('cp.product.list-gallery', ['id' => $id, 'item_id' => $item_id]) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-picture-o"></i> @if($id == 1) Gallery @else Image @endif
							</span>
						</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-banner')" onclick="window.location.href='{{ route('cp.product.list-banner', ['id' => $id, 'item_id' => $item_id]) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-file-image-o"></i> Banner
							</span>
						</a>
					</li>
					
				</ul>
			</div>
		</div><!--.tabs-section-nav-->

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active">
				<div id="tab-content-cnt" class="container-fluid">
					@yield ('tab-item-content')
				</div>
			</div>
		</div><!--.tab-content-->
	</section><!--.tabs-section-->
				
	


@endsection