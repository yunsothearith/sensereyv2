@extends('cp.property.about.tab')
@section ('section-title', 'View Image')
@section ('about-active-image', 'active')
@section ('tab-css')
<link href="{{ asset ('public/user/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/user/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 1000px;
		}
	</style>
@endsection
@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection
@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			
		}); 
		
	</script>
	<script type="text/JavaScript">
		
		var btnCust = ''; 
		$("#photo").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="http://via.placeholder.com/400x270?text=free" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif"]
		});
	
	</script>
@endsection


@section ('about')
	<br />
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route($route.'project-image.create', $id) }}" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right; margin-left: 4px;"><span class="fa fa-plus"></span></a> &nbsp;
			<a href="{{ route($route.'project-image.index', $id) }}" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;"><span class="fa fa-arrow-left"></span></a>
		</div>
	</div><!--.row-->
	@include('cp.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="property_id" value="{{ $id}}">
		<input type="hidden" name="photo_id" value="{{ $data->id}}">

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">Image</label>
			<div class="col-sm-10">
				<div class="kv-avatar center-block">
			        <input id="photo" name="photo" type="file" class="file-loading">
			    </div>
			</div>
		</div>
		@if($data->updater_id != '')
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Update By</label>
				<div class="col-sm-10">
					<input 	id="deposit"
							disabled="" 
							name="deposit"
							value = "{{ $data->cp->en_name }} On {{$data->updated_at}}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
		@endif
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				@if(checkRole($id, 'update-image'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
				@if(checkRole($id, 'delete-image'))<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', ['id'=>$id, 'image_id'=>$data->id]) }}', '{{ route($route.'.index', $id) }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>@endif
			</div>
		</div>
	</form>
	
@endsection