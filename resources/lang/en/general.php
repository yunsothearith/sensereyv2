<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
   

    'home' => 'Home',
    'about' => 'About Us',
    'service' => 'Service',
    'project' => 'Project',
    'team' => 'Our Team',
    'career' => 'Career',
    'gallery' => 'Gallery',
    'blog' => 'Blog',
    'contact' => 'Contact Us',
    'company' => 'Company',
    'brief-profile' => 'BRIEF PROFILE',
    'OVER-15-YEARS-IN-CONSTRUCTION-FIRM' => 'OVER 15 YEARS IN CONSTRUCTION FIRM',
    'Our-Services' => 'OUR SERVICES',
    'Our-Services-senserey' => 'SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company',
    'contact-information' => 'Contact information',
    'mssege' => 'Send Us a Message',
    'project-progress' => 'PROJECT PROGRESS',
    //////////////////////////home
    'complet-project' =>'COMPLETED PROJECTS',
    'company-is-involved-in-every-aspect-of-a-construction-and-experienced' =>'company is involved in every aspect of a construction and experienced.',
    'partner' =>'PARTNERS AND CLIENTS',
    //////////////about
    'COMPANY-PROFILE' => 'COMPANY PROFILE',
    'all-people' => 'All f our engineers are Cambodian and have graduted from famous local and universities,all together we have many years of experience in the conntry as well as abroad.with an increasng number of experiened cambodian staf and construction project',
    'company is involved in every aspect of a construction and experienced.' => 'company is involved in every aspect of a construction and experienced.',
    'certificate-regisration-documents'=>'CERTIFICATE REGISRATION DOCUMENTS',
  /////////////////////////////////project//////////////////
  'company-is-involved-in-every-aspect -of-a-construction-and-experienced.'=>'company is involved in every aspect of a construction and experienced.',
  'all-group' => 'All Groups',
  /////////////////////career/////////////////////////////////////
  'Graphic-Designer-Internship'=>'Graphic Designer Internship(Internship)',
  'We are looking for Graphic Designer Internship. Please read the job announcement below for your information.'=>'We are looking for Graphic Designer Internship. Please read the job announcement below for your information.',
  'read-more'=>'Read More',
  'our-people'=>'OUR PPEOPLE',
  //////////////////////contact///////////////////////////////////
  'location'=>'No. 164, Street Tonlesab, Sangkat Chroy Changva, Khan Russey Keo, Phnom Penh, Cambodia. Phonm Penh',
  'name'=>'Name',
  'email'=>'Email',
  'phone'=>'Phone Number',
  'message'=>'Message',
  /////////////////////////////footer///////////////
  'SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company'=>'SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company',
  '© 2020 SENSEREY CONSTRUCTION CO., LTD.All rights reserved'=>'© 2020 SENSEREY CONSTRUCTION CO., LTD.All rights reserved',
];