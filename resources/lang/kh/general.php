<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
    
    'home' => 'ទំព័រដើម', 
    'about' => 'អំពី​​យើង',
    'service' => 'សេវាកម្ម',
    'project' => 'គម្រោង',
    'team' => 'ក្រុម​របស់​យើង',
    'career' => 'អាជីព',
    'gallery' => 'វិចិត្រសាល',
    'blog' => 'ពត័មាន',
    'contact' => 'ទំនាក់ទំនង',
    'company' => 'ក្រុមហ៊ុន',
    'brief-profile' => 'ប្រវត្តិ',
    'OVER-15-YEARS-IN-CONSTRUCTION-FIRM' => 'ក្នុងរយៈពេល ១៥ ឆ្នាំក្នុងការកសាងសំណង់',
    'Our-Services' => 'សេវាកម្មរបស់យើង',
    'Our-Services-senserey' => 'បានចុះបញ្ជីស្របច្បាប់នៅក្នុងប្រទេសកម្ពុជាជាក្រុមហ៊ុនសំណង់.',
    'company is involved in every aspect of a construction and experienced.' => 'ក្រុមហ៊ុនត្រូវបានចូលរួមនៅក្នុងគ្រប់ផ្នែកនៃសំណង់និងមានបទពិសោធន៍.',
    'contact-information' => 'ព័ត៌មានទំនាក់ទំនង',
    'mssege' => 'ផ្ញើសារមកយើង',
    'project-progress' => 'វឌ្ឍនភាពគម្រោង',
    'complet-project' =>'គម្រោងដែលបានបញ្ចប់',
    ////////////////////home///////////////////////////
    'company-is-involved-in-every-aspect-of-a-construction-and-experienced' =>'ក្រុមហ៊ុនចូលរួមក្នុងវិស័យសំណង់និងបទពិសោធន៍',
    'partner' =>'ដៃគូនិងអតិថិជន',
    /////////////////////about/////////////////////////////////////////
    'COMPANY-PROFILE' => 'ប្រវត្តិ​ក្រុមហ៊ុន',
    'all-people' => 'វិស្វកររបស់យើងទាំងអស់សុទ្ធតែជាជនជាតិខ្មែរហើយបានចាប់យកពីសាកលវិទ្យាល័យល្បី ៗ ក្នុងស្រុកនិងសាកលវិទ្យាល័យ។ យើងទាំងអស់គ្នាមានបទពិសោធន៍ជាច្រើនឆ្នាំនៅក្នុងកប៉ាល់ក៏ដូចជានៅក្រៅប្រទេស',
   'certificate-regisration-documents'=>'ឯកសារបញ្ជាក់អត្តសញ្ញាណប័ណ្ណ',
   ///////////////////////////////Project/////////////////////////////////////////////
   'company-is-involved-in-every-aspect -of-a-construction-and-experienced.'=>'ក្រុមហ៊ុនយើងត្រូវបានចូលរួមនៅក្នុងគ្រប់ផ្នែកនៃសំណង់និងមានបទពិសោធន៍',
   'all-group' => 'ទាំងអស់',
   'Graphic-Designer-Internship'=>'កម្មសិក្សាអ្នករចនាក្រាហ្វិច (កម្មសិក្សា)',
   'We are looking for Graphic Designer Internship. Please read the job announcement below for your information.'=>'យើងកំពុងស្វែងរកកម្មសិក្សាអ្នករចនាក្រាហ្វិច។ សូមអានសេចក្តីប្រកាសការងារខាងក្រោមសម្រាប់ព័ត៌មានរបស់អ្នក',
   'read-more'=>'អានបន្ត',
   'our-people'=>'ក្រុមរបស់យើង',
   //////////////////////contact///////////////////////////////////
   'location'=>'ផ្ទះលេខ ១៦៤ ផ្លូវ Tonelab សង្កាត់ជ្រោយចង្វារខណ្ឌឬស្សីកែវរាជធានីភ្នំពេញ',
   'name'=>'ឈ្មោះ',
   'email'=>'អុីម៉ែល',
   'phone'=>'លេខទូរស័ព្ទ',
   'message'=>'សារ',
   ///////////////////////////footer/////////
   'SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company'=>'SENSEREY CONSTRUCTION CO., LTD. បានចុះបញ្ជីស្របច្បាប់នៅក្នុងប្រទេសកម្ពុជាជាក្រុមហ៊ុនសំណង់',
   '© 2020 SENSEREY CONSTRUCTION CO., LTD.All rights reserved'=>' ២០២០ រក្សា​សិទ្ធិ​គ្រប់​យ៉ាង​ដោយ​ ក្រុមហ៊ុន SENSEREY CONSTRUCTION CO., LTD',
];