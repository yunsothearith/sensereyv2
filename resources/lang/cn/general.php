<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
   
    'home' => '主页',
    'about' => '关于我们',
    'service' => '服務',
    'project' => '項目',
    'team' => '球隊',
    'career' => '事業',
    'gallery' => '畫廊',
    'blog' => '博客',
    'contact' => '聯繫我們',
    'company' => '公司',
    'brief-profile' => '簡介',
    'OVER-15-YEARS-IN-CONSTRUCTION-FIRM' => '超過15年的建築企業',
    'Our-Services' => '我們的服務',
    'Our-Services-senserey' => 'SENSEREY CONSTRUCTION CO。，LTD。 在柬埔寨合法註冊為建築和工程公司',
    'contact-information' => '联系信息',
    'mssege' => '给我们发信息',
    'project-progress' => '项目进度',
    ///////////////////////home
    'complet-project' =>'COMPLETED PROJECTS',
    'partner' =>'PARTNERS AND CLIENTS',
    'company-is-involved-in-every-aspect-of-a-construction-and-experienced' =>'company is involved in every aspect of a construction and experienced.',
    ///////////////about
    'COMPANY-PROFILE' => 'COMPANY PROFILE',
    'all-people' => 'All f our engineers are Cambodian and have graduted from famous local and universities,all together we have many years of experience in the conntry as well as abroad.with an increasng number of experiened cambodian staf and construction project',
    'company is involved in every aspect of a construction and experienced.' => 'company is involved in every aspect of a construction and experienced.',
    'certificate-regisration-documents'=>'CERTIFICATE REGISRATION DOCUMENTS',
     /////////////////////////////////project//////////////////
  'company-is-involved-in-every-aspect -of-a-construction-and-experienced.'=>'company is involved in every aspect of a construction and experienced.',
  'all-group' => 'All Groups',
  'Graphic-Designer-Internship'=>'Graphic Designer Internship(Internship)',
  'We are looking for Graphic Designer Internship. Please read the job announcement below for your information.'=>'We are looking for Graphic Designer Internship. Please read the job announcement below for your information.',
  'read-more'=>'Read More',
  'our-people'=>'OUR PPEOPLE',
  //////////////////////contact///////////////////////////////////
  'location'=>'No. 164, Street Tonlesab, Sangkat Chroy Changva, Khan Russey Keo, Phnom Penh, Cambodia. Phonm Penh',
  'name'=>'Name',
  'email'=>'Email',
  'phone'=>'Phone Number',
  'message'=>'Message',
  'SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company'=>'SENSEREY CONSTRUCTION CO., LTD. has legally registered in Cambodia as the construction and engineering company',
  '© 2020 SENSEREY CONSTRUCTION CO., LTD.All rights reserved'=>'© 2020 SENSEREY CONSTRUCTION CO., LTD.All rights reserved',
];