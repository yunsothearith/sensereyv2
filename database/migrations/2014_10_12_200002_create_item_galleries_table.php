<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_galleries', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->nullable();
            $table->integer('item_id')->unsigned()->index()->nullable();
            $table->integer('type_id')->unsigned()->index()->nullable();
            $table->string('image', 250)->nullable();
            $table->boolean('is_published')->nullable();
            
            //==== Optional Field
            $table->string('code', 550)->nullable();
            $table->string('kh_name', 550)->nullable();       
            $table->string('en_name',550)->nullable();

            $table->string('kh_volume', 550)->nullable();       
            $table->string('en_volume',550)->nullable();

            $table->string('kh_height', 550)->nullable();       
            $table->string('en_height',550)->nullable();

            $table->string('kh_diameter', 550)->nullable();       
            $table->string('en_diameter',550)->nullable();

            $table->string('kh_item_number', 550)->nullable();       
            $table->string('en_item_number',550)->nullable();

            $table->text('kh_content')->nullable();
            $table->text('en_content')->nullable();

           
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_galleries');
    }
}
