<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectprocessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projectprocess', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->nullable();
            $table->string('horizontal_image', 250)->nullable();
            $table->string('en_title', 250)->nullable();
            $table->string('kh_title', 250)->nullable();
            $table->string('cn_title', 250)->nullable();
            $table->text('en_location')->nullable();
            $table->text('kh_location')->nullable();
            $table->text('cn_location')->nullable();
            $table->string('image', 250)->nullable();
            $table->string('vertical_image', 250)->nullable();
            $table->boolean('is_published')->default(0);
            //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectprocess');
    }
}
