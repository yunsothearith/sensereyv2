<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatelogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catelogs', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->nullable();
            $table->integer('product_id')->unsigned()->index()->nullable();
            $table->string('kh_name', 550)->nullable();    
            $table->string('en_name',250)->nullable();
            $table->string('cn_name',250)->nullable();
            $table->string('kh_location', 550)->nullable();    
            $table->string('en_location',250)->nullable();
            $table->string('cn_location',250)->nullable();
            $table->string('image', 250)->nullable();
            $table->boolean('is_published')->nullable();    
            $table->boolean('project_process')->nullable();      
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catelogs');
    }
}
