<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('kh_title', 250)->nullable();
            $table->string('en_title', 250)->nullable();
            $table->string('cn_title', 250)->nullable();

            $table->string('salary', 250)->nullable();

            $table->string('kh_location', 250)->nullable();
            $table->string('en_location', 250)->nullable();
            $table->string('cn_location', 250)->nullable();

            $table->text('kh_description')->nullable();
            $table->text('en_description')->nullable();
            $table->text('cn_description')->nullable();

            $table->text('kh_requirement')->nullable();
            $table->text('en_requirement')->nullable();
            $table->text('cn_requirement')->nullable();

            $table->text('kh_contact')->nullable();
            $table->text('en_contact')->nullable();
            $table->text('cn_contact')->nullable();

            $table->date('deadline')->nullable();
            $table->boolean('is_published')->nullable();

            $table->integer('updater_id')->default(1)->unsigned()->index()->nullable();
            $table->integer('creator_id')->default(1)->unsigned()->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job');
    }
}