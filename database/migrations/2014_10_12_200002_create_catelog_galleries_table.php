<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatelogGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catelog_galleries', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->nullable();
            $table->integer('catelog_id')->unsigned()->index()->nullable();
            $table->string('kh_name', 550)->nullable();    
            $table->string('en_name',250)->nullable();
            $table->string('cn_name',250)->nullable();
            $table->string('kh_description', 550)->nullable();    
            $table->string('en_description',250)->nullable();
            $table->string('cn_description',250)->nullable();
            $table->string('image', 250)->nullable();
            $table->boolean('is_published')->nullable();
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catelog_galleries');
    }
}
