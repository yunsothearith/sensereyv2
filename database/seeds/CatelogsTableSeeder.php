<?php

use Illuminate\Database\Seeder;

class CatelogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('catelogs')->insert(
            [
                [
                    'product_id'          => 1,
                    'kh_name'             => 'RAK Ceramics',
                    'en_name'             => 'RAK Ceramics',
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'        =>    1,
                ],
                [
                    'product_id'          => 2,
                    'kh_name'             => 'RAK Ceramics',
                    'en_name'             => 'RAK Ceramics',
                    'image'               => 'public/frontend/img/products/Ceramics/ALICANTE/ALICANTE.jpg',
                    'is_published'        =>    1,
                ],
                
                [
                    'product_id'          => 3,
                    'kh_name'             => 'RAK Ceramics',
                    'en_name'             => 'RAK Ceramics',
                    'image'               => 'public/frontend/img/products/Ceramics/AMAZON-BLEND-WOOD/AMAZON-BLEND-WOOD.jpg',
                    'is_published'        =>    1,
                ],
                
                [
                    'product_id'          => 1,
                    'kh_name'             => 'RAK Ceramics',
                    'en_name'             => 'RAK Ceramics',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'        =>    1,
                ],
                 [
                    'product_id'          => 2,
                    'kh_name'             => 'RAK Ceramics',
                    'en_name'             => 'RAK Ceramics',
                    'image'               => 'public/frontend/img/products/Ceramics/AMYA/AMYA.jpg',
                    'is_published'        =>    1,
                ],
                 [
                    'product_id'          => 3,
                    'kh_name'             => 'RAK Ceramics',
                    'en_name'             => 'RAK Ceramics',
                    'image'               => 'public/frontend/img/products/Ceramics/APEX/APEX.jpg',
                    'is_published'        =>    1,
                ],
                
            ]
        );

        DB::table('catelog_galleries')->insert(
            [
                [
                    'catelog_id'          => 1,
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'        =>    1,
                ],
                [
                    'catelog_id'          => 2,
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'        =>    1,
                ],
                [
                    'catelog_id'          => 3,
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'        =>    1,
                ],
                [
                    'catelog_id'             => 4,
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'        =>    1,
                ],
                
            ]
        );

	}
}
