<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('items')->insert(
            [
                [
                    'product_id'          => 1,
                    'en_name'             => 'ALCA STONE',
                    'kh_name'             => 'ALCA STONE',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'product_id'          => 1,
                    'en_name'             => 'ALICANTE',
                    'kh_name'             => 'ALICANTE',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/ALICANTE/ALICANTE.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'product_id'          => 1,
                    'en_name'             => 'AMAZON BLEND WOOD',
                    'kh_name'             => 'AMAZON BLEND WOOD',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMAZON-BLEND-WOOD/AMAZON-BLEND-WOOD.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 1,
                    'en_name'             => 'AMBRA CLAY',
                    'kh_name'             => 'AMBRA CLAY',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'All-Cutlery',
                    'kh_name'             => 'All-Cutlery',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'product_id'          => 3,
                    'en_name'             => 'All-Polaris',
                    'kh_name'             => 'All-Polaris',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/ALICANTE/ALICANTE.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'product_id'          => 3,
                    'en_name'             => 'All-Porcelain Hotelware',
                    'kh_name'             => 'All-Porcelain Hotelware',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMAZON-BLEND-WOOD/AMAZON-BLEND-WOOD.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'All-Porcelain Ivory',
                    'kh_name'             => 'All-Porcelain Ivory',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 2,
                    'en_name'             => 'ANNA',
                    'kh_name'             => 'ANNA',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Porcelain/All-Cutlery/ANNA/CANAMCOS.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 2,
                    'en_name'             => 'BANQUET',
                    'kh_name'             => 'BANQUET',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Porcelain/All-Cutlery/ANNA/CANAMCOS.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 2,
                    'en_name'             => 'CLASSIK',
                    'kh_name'             => 'CLASSIK',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Porcelain/All-Cutlery/ANNA/CANAMCOS.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'FINE',
                    'kh_name'             => 'FINE',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Porcelain/All-Cutlery/ANNA/CANAMCOS.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'MASSILIA',
                    'kh_name'             => 'MASSILIA',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Porcelain/All-Cutlery/ANNA/CANAMCOS.jpg',
                    'is_published'      =>    1,
                ],
                
            ]
        );

        DB::table('item_banners')->insert(
            [
                [
                    'item_id'          => 1,
                    'en_name'             => 'Banner',
                    'kh_name'             => 'Banner',
                    'image'               => 'public/frontend/img/rak-ceramics/1.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'item_id'          => 2,
                    'en_name'             => 'Banner',
                    'kh_name'             => 'Banner',
                    'image'               => 'public/frontend/img/rak-ceramics/1.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'item_id'             => 3,
                    'en_name'             => 'Banner',
                    'kh_name'             => 'Banner',
                    'image'               => 'public/frontend/img/rak-ceramics/1.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'item_id'             => 4,
                    'en_name'             => 'Banner',
                    'kh_name'             => 'Banner',
                    'image'               => 'public/frontend/img/rak-ceramics/1.jpg',
                    'is_published'      =>    1,
                ],
                
            ]
        );

        DB::table('item_details')->insert(
            [
                [
                    'item_id'          => 1,
                    'en_name'             => 'Size',
                    'kh_name'             => 'Size',
                    'en_value'             => '120x120 cm',
                    'kh_value'             => '120x120 cm',
                ],
                [
                    'item_id'               => 2,
                    'en_name'               => 'Size',
                    'kh_name'               => 'Size',
                    'en_value'             => '120x120 cm',
                    'kh_value'             => '120x120 cm',
                ],
                [
                    'item_id'          => 2,
                    'en_name'             => 'Size',
                    'kh_name'             => 'Size',
                    'en_value'             => '120x120 cm',
                    'kh_value'             => '120x120 cm',
                ],
                [
                    'item_id'          => 2,
                    'en_name'             => 'Size',
                    'kh_name'             => 'Size',
                    'en_value'             => '120x120 cm',
                    'kh_value'             => '120x120 cm',
                ],
            
                
            ]
        );

        DB::table('item_galleries')->insert(
            [
                [
                    'item_id'          => 1,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                [
                    'item_id'          => 2,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                [
                    'item_id'          => 3,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                [
                    'item_id'          => 4,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],

                [
                    'item_id'          => 1,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                [
                    'item_id'          => 2,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                [
                    'item_id'          => 3,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                [
                    'item_id'          => 4,
                    'image'             => 'public/frontend/img/rak-ceramics/related-product/5.jpg',
                ],
                
            ]
        );

        DB::table('item_colors')->insert(
            [
                [
                    'item_id'          => 1,
                    'kh_color'          => 'Gray',
                    'en_color'          => 'Gray',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/1.jpg',
                ],

                [
                    'item_id'          => 1,
                    'kh_color'          => 'Blue',
                    'en_color'          => 'Blue',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/2.jpg',
                ],
                [
                    'item_id'          => 2,
                    'kh_color'          => 'Gray',
                    'en_color'          => 'Gray',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/1.jpg',
                ],

                [
                    'item_id'          => 2,
                    'kh_color'          => 'Blue',
                    'en_color'          => 'Blue',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/2.jpg',
                ],

                [
                    'item_id'          => 3,
                    'kh_color'          => 'Gray',
                    'en_color'          => 'Gray',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/1.jpg',
                ],

                [
                    'item_id'          => 3,
                    'kh_color'          => 'Blue',
                    'en_color'          => 'Blue',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/2.jpg',
                ],

                [
                    'item_id'          => 4,
                    'kh_color'          => 'Gray',
                    'en_color'          => 'Gray',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/1.jpg',
                ],

                [
                    'item_id'          => 4,
                    'kh_color'          => 'Blue',
                    'en_color'          => 'Blue',
                    'image'             => 'public/frontend/img/rak-ceramics/colors/2.jpg',
                ],
                
            ]
        );

        
        
	}
}
