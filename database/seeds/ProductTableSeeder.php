<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('products')->insert(
            [
                [
                    'en_name'             => 'RAK Ceramics',
                    'kh_name'             => 'RAK Ceramics',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/project/1.jpeg',
                    'is_published'        =>    1,
                    'banner'               => 'public/frontend/img/slider_large1451908937_NAbur.jpg',
                    'en_content'         => 'Our strength is our representation, through our well established distribution network in over 140 countries; a strong presence in all five continents, 
                    that makes us easily available at your call.  With an eye for detail and with our commitment to serve you the best, we would continue to improvise, excel and challenge 
                    our own quality goals to ensure that RAK Porcelain is the preferred choice of Porcelain tableware world over.',
                    'kh_content'        => 'Our strength is our representation, through our well established distribution network in over 140 countries; a strong presence in all five continents, 
                    that makes us easily available at your call.  With an eye for detail and with our commitment to serve you the best, we would continue to improvise, excel and challenge 
                    our own quality goals to ensure that RAK Porcelain is the preferred choice of Porcelain tableware world over.',
                ],
                
                [
                    'en_name'             => 'Stoelzle Lausitz',
                    'kh_name'             => 'Stoelzle Lausitz',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/project/2.jpeg',
                    'is_published'        =>    1,
                    'banner'              => 'public/frontend/img/slider_large1451908937_NAbur.jpg',
                    'en_content'          => 'Our strength is our representation, through our well established distribution network in over 140 countries; a strong presence in all five continents, 
                    that makes us easily available at your call.  With an eye for detail and with our commitment to serve you the best, we would continue to improvise, excel and challenge 
                    our own quality goals to ensure that RAK Porcelain is the preferred choice of Porcelain tableware world over.',
                    'kh_content'          => 'Our strength is our representation, through our well established distribution network in over 140 countries; a strong presence in all five continents, 
                    that makes us easily available at your call.  With an eye for detail and with our commitment to serve you the best, we would continue to improvise, excel and challenge 
                    our own quality goals to ensure that RAK Porcelain is the preferred choice of Porcelain tableware world over.',
                ],

                [
                    'en_name'             => 'Rak Pocelain',
                    'kh_name'             => 'Rak Pocelain',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/project/3.jpeg',
                    'is_published'        =>    1,
                    'banner'              => 'public/frontend/img/slider_large1451908937_NAbur.jpg',
                    'en_content'          => 'Our strength is our representation, through our well established distribution network in over 140 countries; a strong presence in all five continents, 
                    that makes us easily available at your call.  With an eye for detail and with our commitment to serve you the best, we would continue to improvise, excel and challenge 
                    our own quality goals to ensure that RAK Porcelain is the preferred choice of Porcelain tableware world over.',
                    'kh_content'          => 'Our strength is our representation, through our well established distribution network in over 140 countries; a strong presence in all five continents, 
                    that makes us easily available at your call.  With an eye for detail and with our commitment to serve you the best, we would continue to improvise, excel and challenge 
                    our own quality goals to ensure that RAK Porcelain is the preferred choice of Porcelain tableware world over.',
                ],
            ]
        );

        DB::table('product_types')->insert(
            [
                [
                    'product_id'          => 3,
                    'en_name'             => 'New Launch',
                    'kh_name'             => 'New Launch',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/ALCA-STONE/ALCA-STONE.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'product_id'          => 3,
                    'en_name'             => 'Hotelware',
                    'kh_name'             => 'Hotelware',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/ALICANTE/ALICANTE.jpg',
                    'is_published'      =>    1,
                ],
                [
                    'product_id'          => 3,
                    'en_name'             => 'RaK Cutlery',
                    'kh_name'             => 'RaK Cutlery',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMAZON-BLEND-WOOD/AMAZON-BLEND-WOOD.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'Hospital & Healthcare',
                    'kh_name'             => 'Hospital & Healthcare',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'AirLines',
                    'kh_name'             => 'AirLines',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'Retail',
                    'kh_name'             => 'Retail',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'      =>    1,
                ],

                [
                    'product_id'          => 3,
                    'en_name'             => 'Custom Designs',
                    'kh_name'             => 'Custom Designs',
                    'en_description'      => 'discover the collection',
                    'kh_description'      => 'discover the collection',
                    'image'               => 'public/frontend/img/products/Ceramics/AMBRA-CLAY/1.jpg',
                    'is_published'      =>    1,
                ],
                
               
            ]
        );
	}
}
